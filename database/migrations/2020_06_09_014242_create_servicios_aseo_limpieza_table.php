<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiciosAseoLimpiezaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios_aseo_limpieza', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users');
            $table->foreignId('tipo_servicio_id')->constrained('tipos_servicios');
            $table->unsignedInteger('horas_servicio');
            $table->dateTime('fecha_servicio');
            $table->string('direccion', 200);
            $table->string('telefono', 50);
            $table->string('correo_electronico', 200);
            $table->text('observaciones');
            $table->decimal('valor_hora', 18, 2);
            $table->decimal('total', 18,2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicios_aseo_limpieza');
    }
}
