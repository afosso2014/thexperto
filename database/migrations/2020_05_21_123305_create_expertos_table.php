<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpertosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expertos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('persona_id');
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->unsignedBigInteger('area_desempenar_id');
            $table->foreign('area_desempenar_id')->references('id')->on('area_desempenar');
            $table->unsignedBigInteger('nivel_academico_id');
            $table->foreign('nivel_academico_id')->references('id')->on('nivel_academico');
            $table->enum('genero', ['M', 'F', 'O']);
            $table->enum('grupo_sanguineo', ['O+', 'A+', 'B+', 'AB+', 'O-', 'A-', 'B-', 'AB-']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expertos');
    }
}
