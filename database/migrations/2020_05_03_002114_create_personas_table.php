<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id')->unique();
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('tipo_documento_id');
            $table->foreign('tipo_documento_id')->references('id')->on('tipos_documentos');

            $table->unsignedBigInteger('municipio_residencia_id');
            $table->foreign('municipio_residencia_id')->references('id')->on('municipios');

            $table->string('numero_documento', 20);
            $table->string("nombres", 150);
            $table->string('apellidos', 150);
            $table->date('fecha_nacimiento')->default(Carbon::now()->toDateString());
            $table->enum('idioma_nativo', ['E', 'I', 'P']);
            $table->string('direccion', 200);
            $table->string('telefono_fijo', 50)->nullable();
            $table->string('telefono_celular', 50);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
