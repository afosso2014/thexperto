<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Persona;
use Faker\Generator as Faker;

$factory->define(Persona::class, function (Faker $faker) {
    return [
        "user_id" => \App\User::all()->random()->id,
        "tipo_documento_id" => \App\TipoDocumento::all()->random()->id,
        "municipio_residencia_id" => \App\Municipio::all()->random()->id,
        "numero_documento" => $faker->randomNumber(),
        "nombres" => $faker->name(),
        "apellidos" => $faker->lastName(),
        "direccion" => $faker->address(),
        "telefono_fijo" => $faker->phoneNumber(),
        "telefono_celular" => $faker->phoneNumber()
    ];
});
