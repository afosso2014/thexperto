<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'guest'], function(){
    Route::get('/', 'AuthController@retornarVistaLogin')->name('login');
    Route::get('/registrar_usuario', 'AuthController@retornarVistaRegistroUsuario')->name('register.user');
    Route::get('/registrar_experto', 'AuthController@retornarVistaRegistroExperto')->name('RegisterExperto.index');
    Route::get('/obtener_municipio/{departamento_id}', 'MunicipioController@getDataByDepartamento')->name('obtener_municipio');

    Route::post('/login_start', 'AuthController@iniciarSesion')->name('login.start');
    Route::post('/registrar_experto/guardar', 'AuthController@registrarExperto')->name('RegisterExperto.store');
    Route::post('/registrar_usuario/guardar', 'AuthController@registrarUsuario')->name('registeruser.store');


    Route::get('/login/{red_social}', 'AuthController@redirectToProvider')->name('redireccion.redes');
    Route::get('/login/{red_social}/callback', 'AuthController@handleProviderCallback')->name('callback.redes');
});

Route::group(['middleware' => 'auth'], function() {
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::post('/logout', 'DashboardController@logout')->name('logout');
    Route::get('/dashboard/{formulario}/index', 'DashboardController@formsDinamicos')->name("dashboard.redirect");
    Route::get('/export/{tipo}/{modelo}', 'DashboardController@export')->name("dashboard.export");

    /** RUTAS PARA SERVICIO DE ASEO Y LIMPIEZA */
    Route::get('/solicitar_servicio_aseo', 'ServicioAseoLimpiezaController@index')->name('aseolimpieza.index');
    Route::post('/solicitar_servicio_aseo/create', 'ServicioAseoLimpiezaController@create')->name('aseolimpieza.store');
    Route::post('/solicitar_servicio_aseo/data', 'ServicioAseoLimpiezaController@getData')->name('aseolimpieza.data');
    Route::get('/solicitar_servicio_aseo/show/{id}', 'ServicioAseoLimpiezaController@show')->name('aseolimpieza.show');
    Route::put('/solicitar_servicio_aseo/update/{id}', 'ServicioAseoLimpiezaController@update')->name('aseolimpieza.update');

    //** RUTAS PARA ROLES */
    Route::post('/dashboard/roles/getData', 'RolController@getData')->name('roles.get');
    Route::post('/dashboard/roles/create', 'RolController@store')->name('roles.store');
    Route::get('/dashboard/roles/show/{id}', 'RolController@show')->name('roles.show');
    Route::put('/dashboard/roles/update/{id}', 'RolController@update')->name('roles.update');
    Route::delete('/dashboard/roles/delete/{id}', 'RolController@destroy')->name('roles.destroy');

    /** RUTAS PARA USUARIOS */
    Route::post('/dashboard/usuarios/getData', 'UsuarioController@getData')->name('usuarios.get');
    Route::post('/dashboard/usuarios/create', 'UsuarioController@store')->name('usuarios.store');
    Route::get('/dashboard/usuarios/show/{id}', 'UsuarioController@show')->name('usuarios.show');
    Route::put('/dashboard/usuarios/update/{id}', 'UsuarioController@update')->name('usuarios.update');
    Route::delete('/dashboard/usuarios/delete/{id}', 'UsuarioController@destroy')->name('usuarios.destroy');

    /** RUTAS PARA FORMULARIO */
    Route::post('/dashboard/formularios/getData', 'FormularioController@getData')->name('formularios.get');
    Route::post('/dashboard/formularios/create', 'FormularioController@store')->name('formularios.store');
    Route::get('/dashboard/formularios/show/{id}', 'FormularioController@show')->name('formularios.show');
    Route::put('/dashboard/formularios/update/{id}', 'FormularioController@update')->name('formularios.update');
    Route::delete('/dashboard/formularios/delete/{id}', 'FormularioController@destroy')->name('formularios.destroy');

    /** RUTAS PARA ROLES FORMULARIOS */
    Route::post('/dashboard/rolesformularios/getData', 'RolFormularioController@getData')->name('rolesformularios.get');
    Route::post('/dashboard/rolesformularios/create', 'RolFormularioController@store')->name('rolesformularios.store');
    Route::delete('/dashboard/rolesformularios/delete/{id}', 'RolFormularioController@destroy')->name('rolesformularios.destroy');

    /** RUTAS PARA PAISES */
    Route::post('/dashboard/paises/getData', 'PaisController@getData')->name('paises.get');
    Route::post('/dashboard/paises/create', 'PaisController@store')->name('paises.store');
    Route::get('/dashboard/paises/show/{id}', 'PaisController@show')->name('paises.show');
    Route::put('/dashboard/paises/update/{id}', 'PaisController@update')->name('paises.update');
    Route::delete('/dashboard/paises/delete/{id}', 'PaisController@destroy')->name('paises.destroy');

    /** RUTAS PARA DEPARTAMENTOS */
    Route::post('/dashboard/departamentos/getData', 'DepartamentoController@getData')->name('departamentos.get');
    Route::post('/dashboard/departamentos/create', 'DepartamentoController@store')->name('departamentos.store');
    Route::get('/dashboard/departamentos/show/{id}', 'DepartamentoController@show')->name('departamentos.show');
    Route::put('/dashboard/departamentos/update/{id}', 'DepartamentoController@update')->name('departamentos.update');
    Route::delete('/dashboard/departamentos/delete/{id}', 'DepartamentoController@destroy')->name('departamentos.destroy');

    /** RUTAS PARA MUNICIPIOS */
    Route::post('/dashboard/municipios/getData', 'MunicipioController@getData')->name('municipios.get');
    Route::post('/dashboard/municipios/create', 'MunicipioController@store')->name('municipios.store');
    Route::get('/dashboard/municipios/show/{id}', 'MunicipioController@show')->name('municipios.show');
    Route::put('/dashboard/municipios/update/{id}', 'MunicipioController@update')->name('municipios.update');
    Route::delete('/dashboard/municipios/delete/{id}', 'MunicipioController@destroy')->name('municipios.destroy');

    /** RUTAS PARA TIPOS DOCUMENTOS */
    Route::post('/dashboard/tiposdocumentos/getData', 'TipoDocumentoController@getData')->name('tiposdocumentos.get');
    Route::post('/dashboard/tiposdocumentos/create', 'TipoDocumentoController@store')->name('tiposdocumentos.store');
    Route::get('/dashboard/tiposdocumentos/show/{id}', 'TipoDocumentoController@show')->name('tiposdocumentos.show');
    Route::put('/dashboard/tiposdocumentos/update/{id}', 'TipoDocumentoController@update')->name('tiposdocumentos.update');
    Route::delete('/dashboard/tiposdocumentos/delete/{id}', 'TipoDocumentoController@destroy')->name('tiposdocumentos.destroy');

    /** RUTAS PARA TIPOS CUENTAS */
    Route::post('/dashboard/tiposcuentas/getData', 'TipoCuentaController@getData')->name('tiposcuentas.get');
    Route::post('/dashboard/tiposcuentas/create', 'TipoCuentaController@store')->name('tiposcuentas.store');
    Route::get('/dashboard/tiposcuentas/show/{id}', 'TipoCuentaController@show')->name('tiposcuentas.show');
    Route::put('/dashboard/tiposcuentas/update/{id}', 'TipoCuentaController@update')->name('tiposcuentas.update');
    Route::delete('/dashboard/tiposcuentas/delete/{id}', 'TipoCuentaController@destroy')->name('tiposcuentas.destroy');

    /** RUTAS PARA NIVEL ACADEMICO */
    Route::post('/dashboard/nivelesacademicos/getData', 'NivelAcademicoController@getData')->name('nivelesacademicos.get');
    Route::post('/dashboard/nivelesacademicos/create', 'NivelAcademicoController@store')->name('nivelesacademicos.store');
    Route::get('/dashboard/nivelesacademicos/show/{id}', 'NivelAcademicoController@show')->name('nivelesacademicos.show');
    Route::put('/dashboard/nivelesacademicos/update/{id}', 'NivelAcademicoController@update')->name('nivelesacademicos.update');
    Route::delete('/dashboard/nivelesacademicos/delete/{id}', 'NivelAcademicoController@destroy')->name('nivelesacademicos.destroy');

    /** RUTAS PARA BANCOS */
    Route::post('/dashboard/bancos/getData', 'BancoController@getData')->name('bancos.get');
    Route::post('/dashboard/bancos/create', 'BancoController@store')->name('bancos.store');
    Route::get('/dashboard/bancos/show/{id}', 'BancoController@show')->name('bancos.show');
    Route::put('/dashboard/bancos/update/{id}', 'BancoController@update')->name('bancos.update');
    Route::delete('/dashboard/bancos/delete/{id}', 'BancoController@destroy')->name('bancos.destroy');

    /** RUTAS PARA AREA DESEMPENAR */
    Route::post('/dashboard/areasdesempenar/getData', 'AreaDesempenarController@getData')->name('areasdesempenar.get');
    Route::post('/dashboard/areasdesempenar/create', 'AreaDesempenarController@store')->name('areasdesempenar.store');
    Route::get('/dashboard/areasdesempenar/show/{id}', 'AreaDesempenarController@show')->name('areasdesempenar.show');
    Route::put('/dashboard/areasdesempenar/update/{id}', 'AreaDesempenarController@update')->name('areasdesempenar.update');
    Route::delete('/dashboard/areasdesempenar/delete/{id}', 'AreaDesempenarController@destroy')->name('areasdesempenar.destroy');

    /** RUTAS PARA PERFIL */
    Route::get('/profile', 'PersonaController@profile')->name("dashboard.profile");
    Route::put('/profile/update/{id}', 'PersonaController@update')->name("profile.update");

    /** RUTAS PARA TIPO SERVICIO */
    Route::post('/dashboard/tiposervicios/getData', 'TipoServicioController@getData')->name('tiposervicios.get');
    Route::post('/dashboard/tiposervicios/create', 'TipoServicioController@store')->name('tiposervicios.store');
    Route::get('/dashboard/tiposervicios/show/{id}', 'TipoServicioController@show')->name('tiposervicios.show');
    Route::put('/dashboard/tiposervicios/update/{id}', 'TipoServicioController@update')->name('tiposervicios.update');
    Route::delete('/dashboard/tiposervicios/delete/{id}', 'TipoServicioController@destroy')->name('tiposervicios.destroy');
});


