<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'guest'], function() {
    Route::get('registro_usuario/listas', 'AuthController@listas');
    Route::get('obtener_departamento/{idPais}', 'DepartamentoController@getDepartamentoByPais');
    Route::get('obtener_municipio/{departamento_id}', 'MunicipioController@getDataByDepartamento');
    Route::post('guardarUsuario', 'AuthController@registrarUsuarioMobile');
    Route::post('guardarExperto', 'AuthController@registrarExpertoMobile');
    Route::post('iniciarSesion', 'AuthController@iniciarSesionMobile');

});
