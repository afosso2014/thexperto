<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RolFormulario extends Model
{
    use SoftDeletes;

    protected $table = "roles_formularios";
    protected $fillable = ['formulario_id', 'rol_id'];
    public static $encabezadosExportacion = ['Formulario', 'Rol'];

    public function rol() {
        return $this->belongsTo(\App\Rol::class, 'rol_id', 'id');
    }

    public function formulario() {
        return $this->belongsTo(\App\Formulario::class, 'formulario_id', 'id');
    }
}
