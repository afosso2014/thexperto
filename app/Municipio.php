<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Municipio extends Model
{
    use SoftDeletes;

    protected $table = "municipios";
    protected $primaryKey = "id";
    protected $fillable = ['departamento_id', 'codigoDane', 'nombre', 'estado'];
    public static $encabezadosExportacion = ['Departamento', 'Código DANE', 'Nombre', 'Estado'];

    public function departamento() {
        return $this->belongsTo(\App\Departamento::class, 'departamento_id', 'id');
    }
}
