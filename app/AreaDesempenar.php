<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AreaDesempenar extends Model
{
    use SoftDeletes;
    
    protected $table = "area_desempenar";
    protected $fillable = ['codigo', 'nombre', 'estado'];
    public static $encabezadosExportacion = ['Código', 'Nombre', 'Estado'];
}
