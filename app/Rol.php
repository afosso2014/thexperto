<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rol extends Model
{
    use SoftDeletes;

    protected $table = "roles";
    protected $primaryKey = "id";
    protected $fillable = ['nombre', 'estado'];
    public static $encabezadosExportacion = ['Nombre', 'Estado'];

    const EXPERTO = 1;
    const USUARIO = 2;
    const ADMINISTRADOR = 3;
}
