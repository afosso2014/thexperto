<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoCuenta extends Model
{
    use SoftDeletes;

    protected $table = 'tipos_cuentas';
    protected $primaryKey = 'id';
    protected $fillable = ['codigo', 'nombre', 'estado'];
    public static $encabezadosExportacion = ['Código', 'Nombre', 'Estado'];
}
