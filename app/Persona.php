<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Persona extends Model
{
    use SoftDeletes;

    protected $table = "personas";
    protected $primaryKey = "id";
    protected $fillable = [
        'user_id',
        'tipo_documento_id',
        'municipio_residencia_id',
        'numero_documento',
        'nombres',
        'apellidos',
        'fecha_nacimiento',
        'idioma_nativo',
        'direccion',
        'telefono_fijo',
        'telefono_celular'
    ];
}
