<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banco extends Model
{
    use SoftDeletes;

    protected $table = "bancos";
    protected $primaryKey = "id";
    protected $fillable = ['codigo','nombre','estado'];
    public static $encabezadosExportacion = ['Código', 'Nombre', 'Estado'];
}
