<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Formulario extends Model
{
    use SoftDeletes;

    protected $table = "formularios";
    protected $fillable = ['nombre', 'icono', 'url', 'estado'];

    public static $encabezadosExportacion = ['Nombre', 'Icono', 'URL', 'Estado'];
}
