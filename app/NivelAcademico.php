<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NivelAcademico extends Model
{
    use SoftDeletes;
    
    protected $table = "nivel_academico";
    protected $fillable = ['codigo', 'nombre', 'estado'];
    public static $encabezadosExportacion = ['Código', 'Nombre', 'Estado'];
}
