<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Experto extends Model
{
    use SoftDeletes;

    protected $table = "expertos";
    protected $primaryKey = "id";
    protected $fillable = [
        'persona_id',
        'area_desempenar_id',
        'nivel_academico_id',
        'genero',
        'grupo_sanguineo'
    ];
}
