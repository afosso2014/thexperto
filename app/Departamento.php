<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Departamento extends Model
{
    use SoftDeletes;

    protected $table = "departamentos";
    protected $primaryKey = "id";
    protected $fillable = ['pais_id', 'codigoDane', 'nombre', 'estado'];

    public static $encabezadosExportacion = ['Pais', 'Código DANE', 'Nombre', 'Estado'];

    public function pais() {
        return $this->belongsTo(\App\Pais::class, 'pais_id', 'id');
    }
}
