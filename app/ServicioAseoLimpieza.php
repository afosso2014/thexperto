<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServicioAseoLimpieza extends Model
{
    use SoftDeletes;
    
    protected $table = "servicios_aseo_limpieza";
    protected $primaryKey = "id";
    protected $fillable = [
        'user_id',
        'tipo_servicio_id',
        'horas_servicio',
        'fecha_servicio',
        'direccion',
        'telefono',
        'correo_electronico',
        'observaciones',
        'valor_hora',
        'total'
    ];
}
