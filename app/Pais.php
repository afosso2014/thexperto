<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pais extends Model
{
    use SoftDeletes;

    protected $table = 'paises';
    protected $primaryKey = 'id';
    protected $fillable = ['codigoDane', 'nombre', 'estado'];

    public static $encabezadosExportacion = ['Código DANE', 'Nombre', 'Estado'];
}
