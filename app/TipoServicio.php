<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoServicio extends Model
{
    use SoftDeletes;

    protected $table = "tipos_servicios";
    protected $primaryKey = "id";
    protected $fillable = ['area_desempenar_id','nombre','tipo_moneda','precio_hora', 'estado'];
    public static $encabezadosExportacion = ['Area desempeñar','Nombre','Tipo moneda','Precio hora', 'Estado'];

    public function area_desempenar() {
        return $this->belongsTo(\App\AreaDesempenar::class, 'area_desempenar_id', 'id');
    }
}
