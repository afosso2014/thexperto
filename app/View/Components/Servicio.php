<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Servicio extends Component
{
    public $nombreServicio;
    public $colorFondo;
    public $icono;
    public $ruta;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($nombreServicio, $colorFondo, $icono, $ruta)
    {
        $this->nombreServicio = $nombreServicio;
        $this->colorFondo = $colorFondo;
        $this->icono = $icono;
        $this->ruta = $ruta;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.servicio');
    }
}
