<?php

namespace App\View\Components;

use App\RolFormulario;
use Illuminate\View\Component;

class MenuLateral extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        $formularios = RolFormulario::withoutTrashed()
                        ->join('formularios AS f', 'f.id', 'roles_formularios.formulario_id')
                        ->where('roles_formularios.rol_id', auth()->user()->rol_id)
                        ->select('f.*')
                        ->orderBy('f.nombre')
                        ->get();

        return view('components.menu-lateral', compact('formularios'));
    }
}
