<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterPersonaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email:rfc,dns',
            'email_verified' => 'required|email:rfc,dns|same:email',
            'password' => 'required|string|min:3',
            'password_confirmation' => 'required|string|min:3',
            'tipoDocumento' => 'required|exists:tipos_documentos,id',
            'numero_identificacion' => 'required|string',
            'nombres' => 'required|string',
            'apellidos' => 'required|string',
            'nacimiento' => 'required|date',
            'idioma' => 'required|string',
            'telefono_celular' => 'required|string',
            'departamento_residencia' => 'required|exists:departamentos,id',
            'municipio_residencia' => 'required|exists:municipios,id',
            'direccion' => 'required|string'
        ];
    }

    public function messages() {
        return [
            'departamento_residencia.exists' => 'El departamento residencia es invalido',
            'municipio_residencia.exists' => 'El municipio residencia es invalido',
            'tipoDocumento.exists' => 'El tipo de documento es invalido',
            'email.email' => 'Correo electrónico no es valido',
            'email_verified.email' => 'Correo electrónico no es valido',
            'email_verified.same' => 'El correo de confirmación no coincide'
        ];
    }
}
