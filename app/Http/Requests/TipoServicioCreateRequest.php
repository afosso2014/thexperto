<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TipoServicioCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'area_desempenar_id' => 'required|exists:area_desempenar,id',
            'nombre' => 'required|string',
            'tipo_moneda' => 'required|string',
            'precio_hora' => 'required|numeric',
            'estado' => 'required|between:1,2'
        ];
    }
}
