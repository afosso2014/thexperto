<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServicioAseoLimpiezaCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipo_servicio' => 'required|exists:tipos_servicios,id',
            'horas_servicio' => 'required|numeric|min:2',
            'fecha_servicio' => 'required|date',
            'hora_servicio' => 'required',
            'direccion' => 'required|string|max:200',
            'telefono' => 'required|string|max:50',
            'email' => 'required|string|email:rfc,dns|max:200',
            'observaciones' => 'required|string|max:500'
        ];
    }
}
