<?php

namespace App\Http\Controllers;

use App\Http\Requests\TipoDocumentoCreateRequest;
use App\Http\Requests\TipoDocumentoUpdateRequest;
use App\TipoDocumento;
use Illuminate\Http\Request;

class TipoDocumentoController extends Controller
{
    public function getData() {
        $tiposdocumentos = TipoDocumento::withoutTrashed();
        return datatables()
                ->eloquent($tiposdocumentos)
                ->addColumn('btn', '<div class="form-inline"><a href="{{ route("tiposdocumentos.show",$id) }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil-alt"></i> Modificar</a>
                <form action="{{ route("tiposdocumentos.destroy", $id) }}" method="POST" id="frmEliminar{{$id}}">
                    @method("DELETE")
                    @csrf
                </form>
                <button type="button" class="btn btn-danger btn-sm" onclick="confirmacionEliminar({{$id}});"><i class="fa fa-trash"></i> Eliminar</button></div>')
                ->addColumn('estado', function($item) {
                    if($item->estado == 1) {
                        return '<span class="badge badge-success">Activo</span>';
                    } elseif($item->estado == 2) {
                        return '<span class="badge badge-danger">Inactivo</span>';
                    }
                })
                ->rawColumns(['btn', 'estado'])
                ->toJson();
    }

    public function show($id) {
        $tiposdocumentos = TipoDocumento::where('id', $id)->first();
        return view('dashboard.tiposdocumentos.index', compact('tiposdocumentos'));
    }

    public function store(TipoDocumentoCreateRequest $request) {
        try {
            TipoDocumento::create([
                'codigo' => $request->codigo,
                'nombre' => $request->nombre,
                'estado' => $request->estado
            ]);
            return redirect()->route('dashboard.redirect', 'tiposdocumentos')->with('success', 'Registro guardado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function update(TipoDocumentoUpdateRequest $request, $id) {
        try {
            TipoDocumento::where('id', $id)->update([
                'codigo' => $request->codigo,
                'nombre' => $request->nombre,
                'estado' => $request->estado
            ]);
            return redirect()->route('dashboard.redirect', 'tiposdocumentos')->with('success', 'Registro actualizado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function destroy($id) {
        try {
            TipoDocumento::where('id', $id)->delete();
            return redirect()->route('dashboard.redirect', 'tiposdocumentos')->with('success', 'Registro eliminado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
}
