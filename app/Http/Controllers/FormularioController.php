<?php

namespace App\Http\Controllers;

use App\Formulario;
use App\Http\Requests\FormularioCreateRequest;
use App\Http\Requests\FormularioUpdateRequest;
use Illuminate\Http\Request;

class FormularioController extends Controller
{
    public function getData() {
        $formularios = Formulario::withoutTrashed();
        return datatables()
                ->eloquent($formularios)
                ->addColumn('btn', '<div class="form-inline"><a href="{{ route("formularios.show",$id) }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil-alt"></i> Modificar</a>
                <form action="{{ route("formularios.destroy", $id) }}" method="POST" id="frmEliminar{{$id}}">
                    @method("DELETE")
                    @csrf
                </form>
                <button type="button" class="btn btn-danger btn-sm" onclick="confirmacionEliminar({{$id}});"><i class="fa fa-trash"></i> Eliminar</button></div>')
                ->addColumn('estado', function($item) {
                    if($item->estado == 1) {
                        return '<span class="badge badge-success">Activo</span>';
                    } elseif($item->estado == 2) {
                        return '<span class="badge badge-danger">Inactivo</span>';
                    }
                })
                ->addColumn('icono', '<i class="{{$icono}}"></i>')
                ->rawColumns(['btn', 'estado', 'icono'])
                ->toJson();
    }

    public function show($id) {
        $formulario = Formulario::where('id', $id)->first();
        return view('dashboard.formularios.index', compact('formulario'));
    }

    public function store(FormularioCreateRequest $request) {
        try {
            Formulario::create([
                'nombre' => $request->nombre,
                'icono' => $request->icono,
                'url' => $request->url,
                'estado' => $request->estado
            ]);
            return redirect()->route('dashboard.redirect', 'formularios')->with('success', 'Registro guardado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function update(FormularioUpdateRequest $request, $id) {
        try {
            Formulario::where('id', $id)->update([
                'nombre' => $request->nombre,
                'icono' => $request->icono,
                'url' => $request->url,
                'estado' => $request->estado
            ]);
            return redirect()->route('dashboard.redirect', 'formularios')->with('success', 'Registro actualizado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function destroy($id) {
        try {
            Formulario::where('id', $id)->delete();
            return redirect()->route('dashboard.redirect', 'formularios')->with('success', 'Registro eliminado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
}
