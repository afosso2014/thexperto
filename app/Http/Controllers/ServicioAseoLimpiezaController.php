<?php

namespace App\Http\Controllers;

use App\Http\Requests\ServicioAseoLimpiezaCreateRequest;
use App\Persona;
use App\ServicioAseoLimpieza;
use App\TipoServicio;
use Illuminate\Http\Request;

class ServicioAseoLimpiezaController extends Controller
{
    public function index() {
        $persona = Persona::where('user_id', auth()->user()->id)->first();
        $tiposervicios = TipoServicio::withoutTrashed()->where('area_desempenar_id', 1)->get(); //AREA DESEMPENAR DE ASEO Y LIMPIEZA
        return view('dashboard.servicioaseolimpieza.index', compact(['persona', 'tiposervicios']));
    }

    public function create(ServicioAseoLimpiezaCreateRequest $request) {
        try {
            $fechaActual = \Carbon\Carbon::now();
            $fechaSolicitud = \Carbon\Carbon::parse($request->fecha_servicio . ' ' . $request->hora_servicio);

            $diferencia_horas = $fechaSolicitud->diffInHours($fechaActual);
            if($diferencia_horas < 2) {
                return back()->with('error', 'La solicitud la debe realizar con mínimo 2 horas de anticipación')->withInput();
            }

            if($request->tipo_servicio == 1) { // 1 aseo y limpieza general
                if($request->horas_servicio < 2) {
                    return back()->with('error', 'Las horas minimas para el tipo de servicio son 2')->withInput();
                }
            } elseif($request->tipo_servicio == 2) { // 2 aseo y limpieza general con lavanderia
                if($request->horas_servicio < 3) {
                    return back()->with('error', 'Las horas minimas para el tipo de servicio son 3')->withInput();
                }
            }

            $tipoServicio = TipoServicio::withoutTrashed()->where('id', $request->tipo_servicio)->first();

            ServicioAseoLimpieza::create([
                'user_id' => auth()->user()->id,
                'tipo_servicio_id' => $request->tipo_servicio,
                'horas_servicio' => $request->horas_servicio,
                'fecha_servicio' => $request->fecha_servicio . ' ' . $request->hora_servicio,
                'direccion' => $request->direccion,
                'telefono' => $request->telefono,
                'correo_electronico' => $request->email,
                'observaciones' => $request->observaciones,
                'valor_hora' => $tipoServicio->precio_hora,
                'total' => bcmul((string)$tipoServicio->precio_hora, (string)$request->horas_servicio, 2)
            ]);
            
            return redirect('dashboard/misolicitudes/index')->with('success', 'Solicitud guardada con éxito');

        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function getData() {
        $servicioaseolimpieza = ServicioAseoLimpieza::withoutTrashed()
                                ->where('servicios_aseo_limpieza.user_id', auth()->user()->id)
                                ->join('tipos_servicios AS ts', 'ts.id', 'servicios_aseo_limpieza.tipo_servicio_id')
                                ->select(
                                    'servicios_aseo_limpieza.id',
                                    'servicios_aseo_limpieza.horas_servicio',
                                    'servicios_aseo_limpieza.fecha_servicio',
                                    'servicios_aseo_limpieza.direccion',
                                    'servicios_aseo_limpieza.telefono',
                                    'servicios_aseo_limpieza.correo_electronico',
                                    'servicios_aseo_limpieza.observaciones',
                                    'servicios_aseo_limpieza.valor_hora',
                                    'servicios_aseo_limpieza.total',
                                    'ts.nombre'
                                );
        return datatables()
                ->eloquent($servicioaseolimpieza)
                ->addColumn('fecha_servicio', function($item) {
                    $date = date_create($item->fecha_servicio);
                    return date_format($date, 'd-m-Y H:i a');
                })
                ->addColumn('btn', '<div class="form-inline"><a href="{{ route("aseolimpieza.show",$id) }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil-alt"></i> Modificar</a>
                                    <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-money-bill-alt"></i> Realizar pago</a>                    
                                    </div>')
                ->rawColumns(['fecha_servicio', 'btn'])
                ->toJson();
    }

    public function show($id) {
        $tiposervicios = TipoServicio::withoutTrashed()->where('area_desempenar_id', 1)->get();
        $servicio = ServicioAseoLimpieza::where('id', $id)->first();
        return view('dashboard.servicioaseolimpieza.index', compact(['servicio', 'tiposervicios']));   
    }

    public function update(Request $request, $id) {
        try {
            $fechaActual = \Carbon\Carbon::now();
            $fechaSolicitud = \Carbon\Carbon::parse($request->fecha_servicio . ' ' . $request->hora_servicio);

            $diferencia_horas = $fechaSolicitud->diffInHours($fechaActual);
            if($diferencia_horas < 2) {
                return back()->with('error', 'La solicitud la debe realizar con mínimo 2 horas de anticipación')->withInput();
            }

            if($request->tipo_servicio == 1) { // 1 aseo y limpieza general
                if($request->horas_servicio < 2) {
                    return back()->with('error', 'Las horas minimas para el tipo de servicio son 2')->withInput();
                }
            } elseif($request->tipo_servicio == 2) { // 2 aseo y limpieza general con lavanderia
                if($request->horas_servicio < 3) {
                    return back()->with('error', 'Las horas minimas para el tipo de servicio son 3')->withInput();
                }
            }

            $tipoServicio = TipoServicio::withoutTrashed()->where('id', $request->tipo_servicio)->first();

            ServicioAseoLimpieza::where('id', $id)->update([
                'tipo_servicio_id' => $request->tipo_servicio,
                'horas_servicio' => $request->horas_servicio,
                'fecha_servicio' => $request->fecha_servicio . ' ' . $request->hora_servicio,
                'direccion' => $request->direccion,
                'telefono' => $request->telefono,
                'correo_electronico' => $request->email,
                'observaciones' => $request->observaciones,
                'valor_hora' => $tipoServicio->precio_hora,
                'total' => bcmul((string)$tipoServicio->precio_hora, (string)$request->horas_servicio, 2)
            ]);
            
            return redirect('dashboard/misolicitudes/index')->with('success', 'Solicitud actualizada con éxito');

        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
}
