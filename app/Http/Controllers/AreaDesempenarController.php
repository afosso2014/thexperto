<?php

namespace App\Http\Controllers;

use App\AreaDesempenar;
use App\Http\Requests\AreaDesempenarCreateRequest;
use App\Http\Requests\AreaDesempenarUpdateRequest;
use Illuminate\Http\Request;

class AreaDesempenarController extends Controller
{
    public function getData() {
        $areadesempenar = AreaDesempenar::withoutTrashed();
        return datatables()
                ->eloquent($areadesempenar)
                ->addColumn('btn', '<div class="form-inline"><a href="{{ route("areasdesempenar.show",$id) }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil-alt"></i> Modificar</a>
                <form action="{{ route("areasdesempenar.destroy", $id) }}" method="POST" id="frmEliminar{{$id}}">
                    @method("DELETE")
                    @csrf
                </form>
                <button type="button" class="btn btn-danger btn-sm" onclick="confirmacionEliminar({{$id}});"><i class="fa fa-trash"></i> Eliminar</button></div>')
                ->addColumn('estado', function($item) {
                    if($item->estado == 1) {
                        return '<span class="badge badge-success">Activo</span>';
                    } elseif($item->estado == 2) {
                        return '<span class="badge badge-danger">Inactivo</span>';
                    }
                })
                ->rawColumns(['btn', 'estado'])
                ->toJson();
    }

    public function show($id) {
        $areasdesempenar = AreaDesempenar::where('id', $id)->first();
        return view('dashboard.areasdesempenar.index', compact('areasdesempenar'));
    }

    public function store(AreaDesempenarCreateRequest $request) {
        try {
            AreaDesempenar::create([
                'codigo' => $request->codigo,
                'nombre' => $request->nombre,
                'estado' => $request->estado
            ]);
            return redirect()->route('dashboard.redirect', 'areasdesempenar')->with('success', 'Registro guardado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function update(AreaDesempenarUpdateRequest $request, $id) {
        try {
            AreaDesempenar::where('id', $id)->update([
                'codigo' => $request->codigo,
                'nombre' => $request->nombre,
                'estado' => $request->estado
            ]);
            return redirect()->route('dashboard.redirect', 'areasdesempenar')->with('success', 'Registro actualizado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function destroy($id) {
        try {
            AreaDesempenar::where('id', $id)->delete();
            return redirect()->route('dashboard.redirect', 'areasdesempenar')->with('success', 'Registro eliminado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
}
