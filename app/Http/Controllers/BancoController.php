<?php

namespace App\Http\Controllers;

use App\Banco;
use App\Http\Requests\BancoCreateRequest;
use App\Http\Requests\BancoUpdateRequest;
use Illuminate\Http\Request;

class BancoController extends Controller
{
    public function getData() {
        $bancos = Banco::withoutTrashed();
        return datatables()
                ->eloquent($bancos)
                ->addColumn('btn', '<div class="form-inline"><a href="{{ route("bancos.show",$id) }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil-alt"></i> Modificar</a>
                <form action="{{ route("bancos.destroy", $id) }}" method="POST" id="frmEliminar{{$id}}">
                    @method("DELETE")
                    @csrf
                </form>
                <button type="button" class="btn btn-danger btn-sm" onclick="confirmacionEliminar({{$id}});"><i class="fa fa-trash"></i> Eliminar</button></div>')
                ->addColumn('estado', function($item) {
                    if($item->estado == 1) {
                        return '<span class="badge badge-success">Activo</span>';
                    } elseif($item->estado == 2) {
                        return '<span class="badge badge-danger">Inactivo</span>';
                    }
                })
                ->rawColumns(['btn', 'estado'])
                ->toJson();
    }

    public function show($id) {
        $bancos = Banco::where('id', $id)->first();
        return view('dashboard.bancos.index', compact('bancos'));
    }

    public function store(BancoCreateRequest $request) {
        try {
            Banco::create([
                'codigo' => $request->codigo,
                'nombre' => $request->nombre,
                'estado' => $request->estado
            ]);
            return redirect()->route('dashboard.redirect', 'bancos')->with('success', 'Registro guardado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function update(BancoUpdateRequest $request, $id) {
        try {
            Banco::where('id', $id)->update([
                'codigo' => $request->codigo,
                'nombre' => $request->nombre,
                'estado' => $request->estado
            ]);
            return redirect()->route('dashboard.redirect', 'bancos')->with('success', 'Registro actualizado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function destroy($id) {
        try {
            Banco::where('id', $id)->delete();
            return redirect()->route('dashboard.redirect', 'bancos')->with('success', 'Registro eliminado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
}
