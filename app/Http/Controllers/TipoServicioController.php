<?php

namespace App\Http\Controllers;

use App\AreaDesempenar;
use App\Http\Requests\TipoServicioCreateRequest;
use App\Http\Requests\TipoServicioUpdateRequest;
use App\TipoServicio;
use Illuminate\Http\Request;

class TipoServicioController extends Controller
{
    public function getData() {
        $tiposervicios = TipoServicio::withoutTrashed()->with('area_desempenar');
        return datatables()
                ->eloquent($tiposervicios)
                ->addColumn('btn', '<div class="form-inline"><a href="{{ route("tiposervicios.show",$id) }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil-alt"></i> Modificar</a>
                <form action="{{ route("tiposervicios.destroy", $id) }}" method="POST" id="frmEliminar{{$id}}">
                    @method("DELETE")
                    @csrf
                </form>
                <button type="button" class="btn btn-danger btn-sm" onclick="confirmacionEliminar({{$id}});"><i class="fa fa-trash"></i> Eliminar</button></div>')
                ->addColumn('estado', function($item) {
                    if($item->estado == 1) {
                        return '<span class="badge badge-success">Activo</span>';
                    } elseif($item->estado == 2) {
                        return '<span class="badge badge-danger">Inactivo</span>';
                    }
                })
                ->addColumn('area_desempenar', function($item){
                    return $item->area_desempenar->nombre;
                })
                ->rawColumns(['btn', 'estado', 'area_desempenar'])
                ->toJson();
    }

    public function show($id) {
        $tiposervicios = TipoServicio::where('id', $id)->first();
        $listas['area_desempenar'] = AreaDesempenar::withoutTrashed()->get();
        return view('dashboard.tiposervicios.index', compact(['tiposervicios','listas']));
    }

    public function store(TipoServicioCreateRequest $request) {
        try {
            TipoServicio::create([
                'area_desempenar_id' => $request->area_desempenar_id,
                'nombre' => $request->nombre,
                'tipo_moneda' => $request->tipo_moneda,
                'precio_hora' => $request->precio_hora,
                'estado' => $request->estado
            ]);
            return redirect()->route('dashboard.redirect', 'tiposervicios')->with('success', 'Registro guardado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function update(TipoServicioUpdateRequest $request, $id) {
        try {
            TipoServicio::where('id', $id)->update([
                'area_desempenar_id' => $request->area_desempenar_id,
                'nombre' => $request->nombre,
                'tipo_moneda' => $request->tipo_moneda,
                'precio_hora' => $request->precio_hora,
                'estado' => $request->estado
            ]);
            return redirect()->route('dashboard.redirect', 'tiposervicios')->with('success', 'Registro actualizado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function destroy($id) {
        try {
            TipoServicio::where('id', $id)->delete();
            return redirect()->route('dashboard.redirect', 'tiposervicios')->with('success', 'Registro eliminado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
}
