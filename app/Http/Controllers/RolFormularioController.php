<?php

namespace App\Http\Controllers;

use App\Http\Requests\RolFormularioCreateRequest;
use App\RolFormulario;
use Illuminate\Http\Request;

class RolFormularioController extends Controller
{
    public function getData() {
        $rolesformularios = RolFormulario::withoutTrashed()->with(['rol', 'formulario']);
        
        return datatables()
                ->eloquent($rolesformularios)
                ->addColumn('btn', '<div class="form-inline">
                <form action="{{ route("rolesformularios.destroy", $id) }}" method="POST" id="frmEliminar{{$id}}">
                    @method("DELETE")
                    @csrf
                </form>
                <button type="button" class="btn btn-danger btn-sm" onclick="confirmacionEliminar({{$id}});"><i class="fa fa-trash"></i> Eliminar</button></div>')
                ->rawColumns(['btn'])
                ->toJson();
    }

    public function store(RolFormularioCreateRequest $request) {
        try {
            RolFormulario::create([
                'rol_id' => $request->rol,
                'formulario_id' => $request->formulario
            ]);
            return redirect()->route('dashboard.redirect', 'rolesformularios')->with('success', 'Registro guardado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function destroy($id) {
        try {
            RolFormulario::where('id', $id)->delete();
            return redirect()->route('dashboard.redirect', 'rolesformularios')->with('success', 'Registro guardado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
}
