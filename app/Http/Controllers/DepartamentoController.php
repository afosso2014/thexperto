<?php

namespace App\Http\Controllers;

use App\Departamento;
use App\Http\Requests\DepartamentoCreateRequest;
use App\Http\Requests\DepartamentoUpdateRequest;
use App\Pais;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DepartamentoController extends Controller
{
    public function getData() {
        $departamentos = Departamento::withoutTrashed()->join('paises AS p', 'p.id', 'departamentos.pais_id')->select('departamentos.*', DB::raw('p.nombre AS pais'));
        return datatables()
                ->eloquent($departamentos)
                ->addColumn('btn', '<div class="form-inline"><a href="{{ route("departamentos.show",$id) }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil-alt"></i> Modificar</a>
                <form action="{{ route("departamentos.destroy", $id) }}" method="POST" id="frmEliminar{{$id}}">
                    @method("DELETE")
                    @csrf
                </form>
                <button type="button" class="btn btn-danger btn-sm" onclick="confirmacionEliminar({{$id}});"><i class="fa fa-trash"></i> Eliminar</button></div>')
                ->addColumn('estado', function($item) {
                    if($item->estado == 1) {
                        return '<span class="badge badge-success">Activo</span>';
                    } elseif($item->estado == 2) {
                        return '<span class="badge badge-danger">Inactivo</span>';
                    }
                })
                ->rawColumns(['btn', 'estado'])
                ->toJson();
    }

    public function show($id) {
        $departamento = Departamento::where('id', $id)->first();
        $listas['paises'] = Pais::withoutTrashed()->get();
        return view('dashboard.departamentos.index', compact(['departamento', 'listas']));
    }

    public function store(DepartamentoCreateRequest $request) {
        try {
            Departamento::create([
                'pais_id' => $request->pais_id,
                'codigoDane' => $request->codigoDane,
                'nombre' => $request->nombre,
                'estado' => $request->estado
            ]);
            return redirect()->route('dashboard.redirect', 'departamentos')->with('success', 'Registro guardado existosamente');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function update(DepartamentoUpdateRequest $request, $id) {
        try {
            Departamento::where('id', $id)->update([
                'pais_id' => $request->pais_id,
                'codigoDane' => $request->codigoDane,
                'nombre' => $request->nombre,
                'estado' => $request->estado
            ]);
            return redirect()->route('dashboard.redirect', 'departamentos')->with('success', 'Registro actualizado existosamente');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function destroy($id) {
        try {
            Departamento::where('id', $id)->delete();
            return redirect()->route('dashboard.redirect', 'departamentos')->with('success', 'Registro eliminado existosamente');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function getDepartamentoByPais($idPais) {
        return Departamento::withoutTrashed()->where('pais_id', $idPais)->get();
    }
}
