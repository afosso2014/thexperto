<?php

namespace App\Http\Controllers;

use App\Http\Requests\RolCreateRequest;
use App\Http\Requests\RolUpdateRequest;
use App\Rol;
use Illuminate\Http\Request;

class RolController extends Controller
{
    public function getData() {
        $roles = Rol::withoutTrashed();
        return datatables()
                ->eloquent($roles)
                ->addColumn('btn', '<div class="form-inline"><a href="{{ route("roles.show",$id) }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil-alt"></i> Modificar</a>
                <form action="{{ route("roles.destroy", $id) }}" method="POST" id="frmEliminar{{$id}}">
                    @method("DELETE")
                    @csrf
                </form>
                <button type="button" class="btn btn-danger btn-sm" onclick="confirmacionEliminar({{$id}});"><i class="fa fa-trash"></i> Eliminar</button></div>')
                ->addColumn('estado', function($item) {
                    if($item->estado == 1) {
                        return '<span class="badge badge-success">Activo</span>';
                    } elseif($item->estado == 2) {
                        return '<span class="badge badge-danger">Inactivo</span>';
                    }
                })
                ->rawColumns(['btn', 'estado'])
                ->toJson();
    }

    public function show($id) {
        $rol = Rol::where('id', $id)->first();
        return view('dashboard.roles.index', compact('rol'));
    }

    public function store(RolCreateRequest $request) {
        try {
            Rol::create([
                'nombre' => $request->nombre,
                'estado' => $request->estado
            ]);
            return redirect()->route('dashboard.redirect', 'roles')->with('success', 'Registro guardado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function update(RolUpdateRequest $request, $id) {
        try {
            Rol::where('id', $id)->update([
                'nombre' => $request->nombre,
                'estado' => $request->estado
            ]);
            return redirect()->route('dashboard.redirect', 'roles')->with('success', 'Registro actualizado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function destroy($id) {
        try {
            Rol::where('id', $id)->delete();
            return redirect()->route('dashboard.redirect', 'roles')->with('success', 'Registro eliminado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
}
