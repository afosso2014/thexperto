<?php

namespace App\Http\Controllers;

use App\Http\Requests\TipoCuentaCreateRequest;
use App\Http\Requests\TipoCuentaUpdateRequest;
use App\TipoCuenta;
use Illuminate\Http\Request;

class TipoCuentaController extends Controller
{
    public function getData() {
        $tiposcuentas = TipoCuenta::withoutTrashed();
        return datatables()
                ->eloquent($tiposcuentas)
                ->addColumn('btn', '<div class="form-inline"><a href="{{ route("tiposcuentas.show",$id) }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil-alt"></i> Modificar</a>
                <form action="{{ route("tiposcuentas.destroy", $id) }}" method="POST" id="frmEliminar{{$id}}">
                    @method("DELETE")
                    @csrf
                </form>
                <button type="button" class="btn btn-danger btn-sm" onclick="confirmacionEliminar({{$id}});"><i class="fa fa-trash"></i> Eliminar</button></div>')
                ->addColumn('estado', function($item) {
                    if($item->estado == 1) {
                        return '<span class="badge badge-success">Activo</span>';
                    } elseif($item->estado == 2) {
                        return '<span class="badge badge-danger">Inactivo</span>';
                    }
                })
                ->rawColumns(['btn', 'estado'])
                ->toJson();
    }

    public function show($id) {
        $tiposcuentas = TipoCuenta::where('id', $id)->first();
        return view('dashboard.tiposcuentas.index', compact('tiposcuentas'));
    }

    public function store(TipoCuentaCreateRequest $request) {
        try {
            TipoCuenta::create([
                'codigo' => $request->codigo,
                'nombre' => $request->nombre,
                'estado' => $request->estado
            ]);
            return redirect()->route('dashboard.redirect', 'tiposcuentas')->with('success', 'Registro guardado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function update(TipoCuentaUpdateRequest $request, $id) {
        try {
            TipoCuenta::where('id', $id)->update([
                'codigo' => $request->codigo,
                'nombre' => $request->nombre,
                'estado' => $request->estado
            ]);
            return redirect()->route('dashboard.redirect', 'tiposcuentas')->with('success', 'Registro actualizado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function destroy($id) {
        try {
            TipoCuenta::where('id', $id)->delete();
            return redirect()->route('dashboard.redirect', 'tiposcuentas')->with('success', 'Registro eliminado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
}
