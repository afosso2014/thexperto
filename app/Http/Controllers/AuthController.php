<?php

namespace App\Http\Controllers;

use App\Rol;
use App\User;
use App\Pais;
use App\Experto;
use App\Persona;
use App\Departamento;
use App\TipoDocumento;
use App\AreaDesempenar;
use App\NivelAcademico;
use App\Http\Requests\IniciarSesionRequest;
use App\Http\Requests\RegisterExpertoRequest;
use App\Http\Requests\RegisterPersonaRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller
{

    public function retornarVistaLogin() {
        return view('auth.login');
    }

    public function retornarVistaRegistroUsuario() {
        $tiposDocumentos = TipoDocumento::withoutTrashed()->get();
        $departamentos = Departamento::withoutTrashed()->get();
        return view('auth.register_usuario', compact(['tiposDocumentos', 'departamentos']));
    }

    public function retornarVistaRegistroExperto() {
        $tiposDocumentos = TipoDocumento::withoutTrashed()->get();
        $departamentos = Departamento::withoutTrashed()->get();
        $area_desempenar = AreaDesempenar::withoutTrashed()->get();
        $nivel_academico = NivelAcademico::withoutTrashed()->get();
        return view('RegisterExpert.register_experto', compact(['tiposDocumentos', 'departamentos', 'area_desempenar', 'nivel_academico']));
    }

    public function registrarUsuario(RegisterPersonaRequest $request) {
        try {
            DB::beginTransaction();
            $this->guardarUsuario($request);
            DB::commit();
            return back()->with('success', 'Felicitaciones, su registro se ha completado con éxito, ahora puede iniciar sesión');
        } catch(\Exception $e) {
            DB::rollBack();
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function registrarUsuarioMobile(RegisterPersonaRequest $request) {
        try {
            DB::beginTransaction();
            $this->guardarUsuario($request);
            DB::commit();
            return response()->json(['status' => 'success', 'message' => 'Felicitaciones, su registro se ha completado con éxito, ahora puede iniciar sesión']);
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }

    public function registrarExperto(RegisterExpertoRequest $request) {
        try {
            DB::beginTransaction();
            $this->guardarExperto($request);
            DB::commit();
            return back()->with('success', 'Felicitaciones, su registro se ha completado con éxito, ahora puede iniciar sesión');
        } catch(\Exception $e) {
            DB::rollBack();
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function registrarExpertoMobile(RegisterExpertoRequest $request) {
        try {
            DB::beginTransaction();
            $this->guardarExperto($request);
            DB::commit();
            return response()->json(['status' => 'success', 'message' => 'Felicitaciones, su registro se ha completado con éxito, ahora puede iniciar sesión']);
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }

    public function iniciarSesion(IniciarSesionRequest $request) {
        $credentials = $request->only('email', 'password');
        if(Auth::attempt($credentials)) {
            return redirect()->intended('dashboard');
        }
        return back()->with('error', 'Ocurrió un error al autenticar, revise sus credenciales.');
    }

    public function listas() {
        $listas['paises'] = Pais::withoutTrashed()->select('id', 'nombre')->orderBy('nombre')->get();
        $listas['tipos_documentos'] = TipoDocumento::withoutTrashed()->select('id', 'nombre')->orderBy('nombre')->get();
        $listas['area_desempenar'] = AreaDesempenar::withoutTrashed()->select('id', 'nombre')->orderBy('nombre')->get();
        $listas['nivel_academico'] = NivelAcademico::withoutTrashed()->select('id', 'nombre')->orderBy('nombre')->get();
        return response()->json($listas);
    }

    private function guardarUsuario($request) {
        if($request->password != $request->password_confirmation) {
            throw new \Exception("Las contraseñas no coinciden");
        }

        if(User::withoutTrashed()->where('email', $request->email)->exists()) {
            throw new \Exception("Ya existe un usuario con el correo registrado, intente restableciendo la contraseña");
        }

        if(Persona::withoutTrashed()->where('numero_documento', $request->numero_identificacion)->exists()) {
            throw new \Exception("Ya existe una persona registrada con el número de identificación especificado, intente restableciendo la contraseña");
        }

        $usuario = User::create([
            'rol_id' => Rol::EXPERTO, 
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'estado' => 1
        ]);

        Persona::create([
            'user_id' => $usuario->id,
            'tipo_documento_id' => $request->tipoDocumento,
            'municipio_residencia_id' => $request->municipio_residencia,
            'numero_documento' => $request->numero_identificacion,
            'nombres' => $request->nombres,
            'apellidos' => $request->apellidos,
            'fecha_nacimiento' => $request->nacimiento,
            'idioma_nativo' => $request->idioma,
            'direccion' => $request->direccion,
            'telefono_fijo' => $request->telefono_fijo,
            'telefono_celular' => $request->telefono_celular
        ]);
    }

    private function guardarExperto($request) {
        if($request->password != $request->password_confirmation) {
            throw new \Exception('Las contraseñas no coinciden');
        }

        if(User::withoutTrashed()->where('email', $request->email)->exists()) {
            throw new \Exception('Ya existe un usuario con el correo registrado, intente restableciendo la contraseña');
        }

        if(Persona::withoutTrashed()->where('numero_documento', $request->numero_identificacion)->exists()) {
            throw new \Exception('Ya existe una persona registrada con el número de identificación especificado, intente restableciendo la contraseña');
        }

        $usuario = User::create([
            'rol_id' => Rol::EXPERTO, 
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'estado' => 2
        ]);

        $persona = Persona::create([
            'user_id' => $usuario->id,
            'tipo_documento_id' => $request->tipoDocumento,
            'municipio_residencia_id' => $request->municipio_residencia,
            'numero_documento' => $request->numero_identificacion,
            'nombres' => $request->nombres,
            'apellidos' => $request->apellidos,
            'fecha_nacimiento' => $request->nacimiento,
            'idioma_nativo' => $request->idioma,
            'direccion' => $request->direccion,
            'telefono_fijo' => $request->telefono_fijo,
            'telefono_celular' => $request->telefono_celular
        ]);

        Experto::create([
            'persona_id' => $persona->id,
            'area_desempenar_id' => $request->area_desempenar,
            'nivel_academico_id' => $request->nivel_academico,
            'genero' => $request->genero,
            'grupo_sanguineo' => $request->grupo_sanguineo
        ]);
    }

    public function iniciarSesionMobile(IniciarSesionRequest $request)
    {
        $usuario = User::withoutTrashed()->where('email', $request->email)->first();
        if(!isset($usuario)) {
            return response()->json(['status' => 'error', 'message' => 'No existe un usuario registrado con el correo electronico']);
        }

        $credentials = $request->only('email', 'password');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['status' => 'error', 'message' => 'Credenciales invalidas']);
            }
        } catch (JWTException $e) {
            return response()->json(['status' => 'error', 'message' => 'No se pudo iniciar sesión, intente de nuevo mas tarde']);
        }
        
        return response()->json(['status' => 'success', 'token' => $token, 'usuario' => $usuario]);
    }

    public function redirectToProvider($red_social) {
        return Socialite::driver($red_social)->redirect();
    }

    public function handleProviderCallback($red_social) {
        dd(Socialite::driver($red_social)->user());
    }
}
