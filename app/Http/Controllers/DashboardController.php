<?php

namespace App\Http\Controllers;

use App\AreaDesempenar;
use App\Departamento;
use App\Exports\ExportsGeneral;
use App\Formulario;
use App\Pais;
use App\Persona;
use App\Rol;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class DashboardController extends Controller
{
    public function index() {
        return view('dashboard.index');
    }

    public function logout() {
        Auth::logout();
        return redirect()->route('login');
    }

    public function formsDinamicos($formulario) {
        $listas = [];

        switch($formulario) {
            case "usuarios":
                $listas = [
                    'roles' => Rol::withoutTrashed()->get()
                ];
                break;
            case "rolesformularios":
                $listas = [
                    'roles' => Rol::withoutTrashed()->get(),
                    'formularios' => Formulario::withoutTrashed()->get()
                ];
                break;
            case "departamentos":
                $listas = [
                    'paises' => Pais::withoutTrashed()->get()
                ];
                break;
            case "municipios":
                $listas = [
                    'departamentos' => Departamento::withoutTrashed()->get()
                ];
                break;
            case "tiposervicios":
                $listas = [
                    'area_desempenar' => AreaDesempenar::withoutTrashed()->get()
                ];
                break;
            case "misolicitudes":
                $listas = [
                    'area_desempenar' => AreaDesempenar::withoutTrashed()->get()
                ];
                break;
        }

        return view('dashboard.' . $formulario . '.index', compact('listas'));
    }

    public function export($tipo, $modelo) {
        switch($tipo) {
            case "csv":
                return Excel::download(new ExportsGeneral($modelo), $modelo.'.csv');
            case "excel":
                return Excel::download(new ExportsGeneral($modelo), $modelo.'.xlsx');
            case "pdf":
                return Excel::download(new ExportsGeneral($modelo), $modelo.'.pdf');
        }
    }
}
