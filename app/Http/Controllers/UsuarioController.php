<?php

namespace App\Http\Controllers;

use App\Http\Requests\UsuarioCreateRequest;
use App\Http\Requests\UsuarioUpdateRequest;
use App\Rol;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UsuarioController extends Controller
{
    public function getData() {
        $usuarios = User::withoutTrashed()->with('roles');
        return datatables()
                ->eloquent($usuarios)
                ->addColumn('btn', '<div class="form-inline"><a href="{{ route("usuarios.show",$id) }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil-alt"></i> Modificar</a>
                <form action="{{ route("usuarios.destroy", $id) }}" method="POST" id="frmEliminar{{$id}}">
                    @method("DELETE")
                    @csrf
                </form>
                <button type="button" class="btn btn-danger btn-sm" onclick="confirmacionEliminar({{$id}});"><i class="fa fa-trash"></i> Eliminar</button></div>')
                ->addColumn('estado', function($item) {
                    if($item->estado == 1) {
                        return '<span class="badge badge-success">Activo</span>';
                    } elseif($item->estado == 2) {
                        return '<span class="badge badge-danger">Inactivo</span>';
                    }
                })
                ->rawColumns(['btn', 'estado'])
                ->toJson();
    }

    public function store(UsuarioCreateRequest $request) {
        try {
            User::create([
                'rol_id' => $request->rol_id,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'estado' => $request->estado
            ]);
            return redirect()->route('dashboard.redirect', 'usuarios')->with('success', 'Usuario creado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function show($id) {
        $usuario = User::where('id', $id)->first();
        $listas['roles'] = Rol::withoutTrashed()->get();
        return view('dashboard.usuarios.index', compact(['usuario', 'listas']));
    }

    public function update(UsuarioUpdateRequest $request, $id) {
        try {
            $usuario = User::where('id', $id)->first();
            $usuario->rol_id = $request->rol_id;
            $usuario->email = $request->email;
            $usuario->estado = $request->estado;

            if(isset($request->password)) {
                $usuario->password = Hash::make($request->password);
            }

            $usuario->save();

            // User::where('id', $id)->update([
            //     'rol_id' => $request->rol_id,
            //     'email' => $request->email,
            //     'password' => Hash::make($request->password),
            //     'estado' => $request->estado
            // ]);
            return redirect()->route('dashboard.redirect', 'usuarios')->with('success', 'Usuario actualizado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function destroy($id) {
        try {
            User::where('id', $id)->delete();
            return redirect()->route('dashboard.redirect', 'usuarios')->with('success', 'Usuario eliminado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        return response()->json(compact('token'));
    }

    public function getAuthenticatedUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                    return response()->json(['user_not_found'], 404);
            }
            } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                    return response()->json(['token_expired'], $e->getStatusCode());
            } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                    return response()->json(['token_invalid'], $e->getStatusCode());
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
                    return response()->json(['token_absent'], $e->getStatusCode());
            }
            return response()->json(compact('user'));
    }
}
