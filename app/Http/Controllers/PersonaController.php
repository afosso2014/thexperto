<?php

namespace App\Http\Controllers;

use App\Departamento;
use App\Municipio;
use App\Pais;
use App\Persona;
use App\TipoDocumento;
use Illuminate\Http\Request;

class PersonaController extends Controller
{
    public function profile() {
        $persona = Persona::withoutTrashed() 
                    ->join('municipios AS m', 'm.id', 'personas.municipio_residencia_id')
                    ->join('departamentos AS d', 'd.id', 'm.departamento_id')
                    ->select('personas.*', 'm.id AS municipio_id', 'd.id AS departamento_id', 'd.pais_id')
                    ->where('user_id', auth()->user()->id)->first();
        $tipoDocumentos = TipoDocumento::withoutTrashed()->get();
        $paises = Pais::withoutTrashed()->get();
        $departamentos = Departamento::withoutTrashed()->get();
        $municipios = Municipio::withoutTrashed()->get();
        return view('dashboard.profile.profile', compact(['persona', 'tipoDocumentos', 'paises', 'departamentos', 'municipios']));
    }

    public function update(Request $request, $id) {
        try {
            Persona::where('id', $id)->update([
                'tipo_documento_id' => $request->tipo_documento,
                'numero_documento' => $request->numero_documento,
                'direccion' => $request->direccion,
                'telefono_fijo' => $request->telefono_fijo,
                'telefono_celular' => $request->telefono_celular
            ]);
            return back()->with('success', 'Perfil actualizado exitosamente');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }
}
