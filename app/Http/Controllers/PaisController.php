<?php

namespace App\Http\Controllers;

use App\Http\Requests\PaisCreateRequest;
use App\Http\Requests\PaisUpdateRequest;
use App\Pais;
use Illuminate\Http\Request;

class PaisController extends Controller
{
    public function getData() {
        $paises = Pais::withoutTrashed();
        return datatables()
                ->eloquent($paises)
                ->addColumn('btn', '<div class="form-inline"><a href="{{ route("paises.show",$id) }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil-alt"></i> Modificar</a>
                <form action="{{ route("paises.destroy", $id) }}" method="POST" id="frmEliminar{{$id}}">
                    @method("DELETE")
                    @csrf
                </form>
                <button type="button" class="btn btn-danger btn-sm" onclick="confirmacionEliminar({{$id}});"><i class="fa fa-trash"></i> Eliminar</button></div>')
                ->addColumn('estado', function($item) {
                    if($item->estado == 1) {
                        return '<span class="badge badge-success">Activo</span>';
                    } elseif($item->estado == 2) {
                        return '<span class="badge badge-danger">Inactivo</span>';
                    }
                })
                ->rawColumns(['btn', 'estado'])
                ->toJson();
    }

    public function show($id) {
        $pais = Pais::withoutTrashed()->where('id', $id)->first();
        return view('dashboard.paises.index', compact('pais'));
    }

    public function store(PaisCreateRequest $request) {
        try {
            Pais::create([
                'codigoDane' => $request->codigoDane,
                'nombre' => $request->nombre,
                'estado' => $request->estado
            ]);
            return redirect()->route('dashboard.redirect', 'paises')->with('success', 'Registro guardado existosamente');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function update(PaisUpdateRequest $request, $id) {
        try {
            Pais::where('id', $id)->update([
                'codigoDane' => $request->codigoDane,
                'nombre' => $request->nombre,
                'estado' => $request->estado
            ]);
            return redirect()->route('dashboard.redirect', 'paises')->with('success', 'Registro actualizado existosamente');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function destroy($id) {
        try {
            Pais::where('id', $id)->delete();
            return redirect()->route('dashboard.redirect', 'paises')->with('success', 'Registro eliminado existosamente');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
}
