<?php

namespace App\Http\Controllers;

use App\Departamento;
use App\Http\Requests\MunicipioCreateRequest;
use App\Http\Requests\MunicipioUpdateRequest;
use App\Municipio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MunicipioController extends Controller
{
    public function getDataByDepartamento($departamento_id) {
        return response()->json(Municipio::withoutTrashed()->where('departamento_id', $departamento_id)->get());
    }

    public function getData() {
        $municipios = Municipio::withoutTrashed()->join('departamentos AS d', 'd.id', 'municipios.departamento_id')->select('municipios.*', DB::raw('d.nombre AS departamento'));
        return datatables()
                ->eloquent($municipios)
                ->addColumn('btn', '<div class="form-inline"><a href="{{ route("municipios.show",$id) }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil-alt"></i> Modificar</a>
                <form action="{{ route("municipios.destroy", $id) }}" method="POST" id="frmEliminar{{$id}}">
                    @method("DELETE")
                    @csrf
                </form>
                <button type="button" class="btn btn-danger btn-sm" onclick="confirmacionEliminar({{$id}});"><i class="fa fa-trash"></i> Eliminar</button></div>')
                ->addColumn('estado', function($item) {
                    if($item->estado == 1) {
                        return '<span class="badge badge-success">Activo</span>';
                    } elseif($item->estado == 2) {
                        return '<span class="badge badge-danger">Inactivo</span>';
                    }
                })
                ->rawColumns(['btn', 'estado'])
                ->toJson();
    }

    public function show($id) {
        $municipio = Municipio::where('id', $id)->first();
        $listas['departamentos'] = Departamento::withoutTrashed()->get();
        return view('dashboard.municipios.index', compact(['municipio', 'listas']));
    }

    public function store(MunicipioCreateRequest $request) {
        try {
            Municipio::create([
                'departamento_id' => $request->departamento_id,
                'codigoDane' => $request->codigoDane,
                'nombre' => $request->nombre,
                'estado' => $request->estado
            ]);
            return redirect()->route('dashboard.redirect', 'municipios')->with('success', 'Registro guardado existosamente');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function update(MunicipioUpdateRequest $request, $id) {
        try {
            Municipio::where('id', $id)->update([
                'departamento_id' => $request->departamento_id,
                'codigoDane' => $request->codigoDane,
                'nombre' => $request->nombre,
                'estado' => $request->estado
            ]);
            return redirect()->route('dashboard.redirect', 'municipios')->with('success', 'Registro actualizado existosamente');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function destroy($id) {
        try {
            Municipio::where('id', $id)->delete();
            return redirect()->route('dashboard.redirect', 'municipios')->with('success', 'Registro eliminado existosamente');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
}
