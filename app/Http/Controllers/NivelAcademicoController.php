<?php

namespace App\Http\Controllers;

use App\Http\Requests\NivelAcademicoCreateRequest;
use App\Http\Requests\NivelAcademicoUpdateRequest;
use App\NivelAcademico;
use Illuminate\Http\Request;

class NivelAcademicoController extends Controller
{
    public function getData() {
        $nivelesacademicos = NivelAcademico::withoutTrashed();
        return datatables()
                ->eloquent($nivelesacademicos)
                ->addColumn('btn', '<div class="form-inline"><a href="{{ route("nivelesacademicos.show",$id) }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil-alt"></i> Modificar</a>
                <form action="{{ route("nivelesacademicos.destroy", $id) }}" method="POST" id="frmEliminar{{$id}}">
                    @method("DELETE")
                    @csrf
                </form>
                <button type="button" class="btn btn-danger btn-sm" onclick="confirmacionEliminar({{$id}});"><i class="fa fa-trash"></i> Eliminar</button></div>')
                ->addColumn('estado', function($item) {
                    if($item->estado == 1) {
                        return '<span class="badge badge-success">Activo</span>';
                    } elseif($item->estado == 2) {
                        return '<span class="badge badge-danger">Inactivo</span>';
                    }
                })
                ->rawColumns(['btn', 'estado'])
                ->toJson();
    }

    public function show($id) {
        $nivelesacademicos = NivelAcademico::where('id', $id)->first();
        return view('dashboard.nivelesacademicos.index', compact('nivelesacademicos'));
    }

    public function store(NivelAcademicoCreateRequest $request) {
        try {
            NivelAcademico::create([
                'codigo' => $request->codigo,
                'nombre' => $request->nombre,
                'estado' => $request->estado
            ]);
            return redirect()->route('dashboard.redirect', 'nivelesacademicos')->with('success', 'Registro guardado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function update(NivelAcademicoUpdateRequest $request, $id) {
        try {
            NivelAcademico::where('id', $id)->update([
                'codigo' => $request->codigo,
                'nombre' => $request->nombre,
                'estado' => $request->estado
            ]);
            return redirect()->route('dashboard.redirect', 'nivelesacademicos')->with('success', 'Registro actualizado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function destroy($id) {
        try {
            NivelAcademico::where('id', $id)->delete();
            return redirect()->route('dashboard.redirect', 'nivelesacademicos')->with('success', 'Registro eliminado con éxito');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
}
