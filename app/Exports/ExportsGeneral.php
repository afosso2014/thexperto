<?php

namespace App\Exports;

use App\AreaDesempenar;
use App\Banco;
use App\Departamento;
use App\Formulario;
use App\Municipio;
use App\NivelAcademico;
use App\Pais;
use App\Rol;
use App\RolFormulario;
use App\TipoCuenta;
use App\TipoDocumento;
use App\TipoServicio;
use App\User;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportsGeneral implements FromCollection, WithHeadings {

    private $modelo;

    public function __construct($modelo) {
        $this->modelo = $modelo;
    }

    public function collection() {
        switch($this->modelo) {
            case "formularios":
                return collect(Formulario::withoutTrashed()->get())->transform(function($item) {
                    return [
                        $item->nombre,
                        $item->icono,
                        $item->url,
                        $this->retornoEstado($item->estado)
                    ];
                });
            case "usuarios":
                return collect(User::withoutTrashed()->with('roles')->get())->transform(function($item) {
                    return [
                        $item->email,
                        $item->roles->nombre,
                        $this->retornoEstado($item->estado)
                    ];
                });
            case "roles":
                return collect(Rol::withoutTrashed()->get())->transform(function($item) {
                    return [
                        $item->nombre,
                        $this->retornoEstado($item->estado)
                    ];
                });
            case "rolesformularios":
                return collect(RolFormulario::withoutTrashed()->with(['rol', 'formulario'])->get())->transform(function($item) {
                    return [
                        $item->rol->nombre,
                        $item->formulario->nombre
                    ];
                });
            case "paises":
                return collect(Pais::withoutTrashed()->get())->transform(function($item) {
                    return [
                        $item->codigoDane,
                        $item->nombre,
                        $this->retornoEstado($item->estado)
                    ];
                });
            case "departamentos":
                return collect(Departamento::withoutTrashed()->join('paises AS p', 'p.id', 'departamentos.pais_id')->select('departamentos.*', DB::raw('p.nombre AS pais'))->get())->transform(function($item) {
                    return [
                        $item->pais,
                        $item->codigoDane,
                        $item->nombre,
                        $this->retornoEstado($item->estado)
                    ];
                });
            case "municipios":
                return collect(Municipio::withoutTrashed()->join('departamentos AS d', 'd.id', 'municipios.departamento_id')->select('municipios.*', DB::raw('d.nombre AS departamento'))->get())->transform(function($item) {
                    return [
                        $item->departamento,
                        $item->codigoDane,
                        $item->nombre,
                        $this->retornoEstado($item->estado)
                    ];
                });
            case "tiposdocumentos":
                return collect(TipoDocumento::withoutTrashed()->get())->transform(function($item) {
                    return [
                        $item->codigo,
                        $item->nombre,
                        $this->retornoEstado($item->estado)
                    ];
                });
            case "tiposcuentas":
                return collect(TipoCuenta::withoutTrashed()->get())->transform(function($item) {
                    return [
                        $item->codigo,
                        $item->nombre,
                        $this->retornoEstado($item->estado)
                    ];
                });
            case "nivelesacademicos":
                return collect(NivelAcademico::withoutTrashed()->get())->transform(function($item) {
                    return [
                        $item->codigo,
                        $item->nombre,
                        $this->retornoEstado($item->estado)
                    ];
                });
            case "bancos":
                return collect(Banco::withoutTrashed()->get())->transform(function($item) {
                    return [
                        $item->codigo,
                        $item->nombre,
                        $this->retornoEstado($item->estado)
                    ];
                });
            case "areasdesempenar":
                return collect(AreaDesempenar::withoutTrashed()->get())->transform(function($item) {
                    return [
                        $item->codigo,
                        $item->nombre,
                        $this->retornoEstado($item->estado)
                    ];
                });
            case "tiposervicios":
                return collect(TipoServicio::withoutTrashed()->with('area_desempenar')->get())->transform(function($item) {
                    return [
                        $item->area_desempenar->nombre,
                        $item->nombre,
                        $item->tipo_moneda,
                        $item->precio_hora,
                        $this->retornoEstado($item->estado)
                    ];
                });
        }
    }

    public function headings(): array {
        switch($this->modelo) {
            case "formularios":
                return Formulario::$encabezadosExportacion;
            case "usuarios":
                return User::$encabezadosExportacion;
            case "roles":
                return Rol::$encabezadosExportacion;
            case "rolesformularios":
                return RolFormulario::$encabezadosExportacion;
            case "paises":
                return Pais::$encabezadosExportacion;
            case "departamentos":
                return Departamento::$encabezadosExportacion;
            case "municipios":
                return Municipio::$encabezadosExportacion;
            case "tiposdocumentos":
                return TipoDocumento::$encabezadosExportacion;
            case "tiposcuentas":
                return TipoCuenta::$encabezadosExportacion;
            case "nivelesacademicos":
                return NivelAcademico::$encabezadosExportacion;
            case "bancos":
                return Banco::$encabezadosExportacion;
            case "areasdesempenar":
                return AreaDesempenar::$encabezadosExportacion;
            case "tiposervicios":
                return TipoServicio::$encabezadosExportacion;
            default:
                return [];
        }
    }

    public function retornoEstado($estado) {
        return $estado == 1 ? "ACTIVO" : "INACTIVO";
    }
}