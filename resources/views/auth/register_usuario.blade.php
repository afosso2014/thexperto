@extends('layouts.app')

@section('content')
<div class="header bg-gradient-primary py-7 py-lg-8">
    <div class="container">
        <div class="header-body text-center">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-6">
                    <h1 class="text-white">{{ trans('labels.welcome') }}</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
        </svg>
    </div>
</div>

<div class="container mt--8 pb-5">
    <div class="row justify-content-center">
        <div class="col-lg-10 col-md-12">
            <div class="card bg-secondary shadow border-0">
                <div class="card-body px-lg-5 py-lg-5">
                    <div class="text-center text-muted mb-4">
                        <small>{{ trans('labels.fill_data_user') }}</small>
                    </div>
                    <form method="POST" action="{{ route('registeruser.store') }}">
                        @csrf

                        <div class="row">
                            <div class="form-group col-md-6 col-lg-6">
                                <label>{{ trans('labels.type_document') }}</label>
                                <div class="input-group input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-certificate"></i></span>
                                    </div>
                                    <select name="tipoDocumento" id="tipoDocumento" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}">
                                        @foreach($tiposDocumentos as $dato)
                                            {!! "<option value='$dato->id'>$dato->nombre</option>" !!}
                                        @endforeach
                                    </select>

                                    @if ($errors->has('tipoDocumento'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('tipoDocumento') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group col-md-6 col-lg-6">
                                <label>{{ trans('labels.number_document') }}</label>
                                <div class="input-group input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-certificate"></i></span>
                                    </div>
                                    <input placeholder="{{ trans('labels.number_document') }}" id="numero_identificacion" type="number" class="form-control{{ $errors->has('numero_identificacion') ? ' is-invalid' : '' }}" name="numero_identificacion" value="{{ old('numero_identificacion') }}" required>

                                    @if ($errors->has('numero_identificacion'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('numero_identificacion') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6 col-lg-6">
                                <label>{{ trans('labels.general.names') }}</label>
                                <div class="input-group input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-certificate"></i></span>
                                    </div>
                                    <input placeholder="{{ trans('labels.general.names') }}" id="nombres" type="text" class="form-control{{ $errors->has('nombres') ? ' is-invalid' : '' }}" name="nombres" value="{{ old('nombres') }}" required>

                                    @if ($errors->has('nombres'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nombres') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group col-md-6 col-lg-6">
                                <label>{{ trans('labels.general.last_names') }}</label>
                                <div class="input-group input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-certificate"></i></span>
                                    </div>
                                    <input placeholder="{{ trans('labels.general.last_names') }}" id="apellidos" type="text" class="form-control{{ $errors->has('apellidos') ? ' is-invalid' : '' }}" name="apellidos" value="{{ old('apellidos') }}" required>

                                    @if ($errors->has('apellidos'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('apellidos') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6 col-lg-6">
                                <label>{{ trans('labels.date_born') }}</label>
                                <div class="input-group input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-certificate"></i></span>
                                    </div>
                                    <input placeholder="{{__('Fecha de Nacimiento')}}" id="nacimiento" type="date" class="form-control{{ $errors->has('nacimiento') ? ' is-invalid' : '' }}" name="nacimiento" value="{{ old('nacimiento') }}" required>

                                    @if ($errors->has('nacimiento'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nacimiento') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group col-md-6 col-lg-6">
                                <label>{{ trans('labels.language') }}</label>
                                <div class="input-group input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-certificate"></i></span>
                                    </div>
                                    <select name="idioma" id="idioma" class="form-control{{ $errors->has('idioma') ? ' is-invalid' : '' }}">
                                        <option value="E">Español</option>
                                        <option value="I">Ingles</option>
                                        <option value="P">Portugues</option>
                                    </select>

                                    @if ($errors->has('idioma'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('idioma') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6 col-lg-6">
                                <label>{{ trans('labels.telefono_fijo') }}</label>
                                <div class="input-group input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-certificate"></i></span>
                                    </div>
                                    <input placeholder="{{ trans('labels.telefono_fijo') }}" id="telefono_fijo" type="number" class="form-control{{ $errors->has('telefono_fijo') ? ' is-invalid' : '' }}" name="telefono_fijo" value="{{ old('telefono_fijo') }}">

                                    @if ($errors->has('telefono_fijo'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('telefono_fijo') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group col-md-6 col-lg-6">
                                <label>{{ trans('labels.telefono_celular') }}</label>
                                <div class="input-group input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-certificate"></i></span>
                                    </div>
                                    <input placeholder="{{ trans('labels.telefono_celular') }}" id="telefono_celular" type="number" class="form-control{{ $errors->has('telefono_celular') ? ' is-invalid' : '' }}" name="telefono_celular" value="{{ old('telefono_celular') }}" required>

                                    @if ($errors->has('telefono_celular'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('telefono_celular') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-4 col-lg-4">
                                <label>{{ trans('labels.department') }} Residencia</label>
                                <div class="input-group input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-certificate"></i></span>
                                    </div>
                                    <select name="departamento_residencia" id="departamento_residencia" class="form-control{{ $errors->has('departamento_residencia') ? ' is-invalid' : '' }}" onchange="getMunicipios();">
                                        <option value='-1'>--Seleccione un departamento--</option>
                                        @foreach($departamentos as $item)
                                            {!! "<option value='$item->id'>$item->nombre</option>" !!}
                                        @endforeach
                                    </select>

                                    @if ($errors->has('departamento_residencia'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('departamento_residencia') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group col-md-4 col-lg-4">
                                <label>{{ trans('labels.municipio') }} Residencia</label>
                                <div class="input-group input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-certificate"></i></span>
                                    </div>
                                    <select name="municipio_residencia" id="municipio_residencia" class="form-control{{ $errors->has('municipio_residencia') ? ' is-invalid' : '' }}">
                                        <option value="-1">--Seleccione su municipio de residencia--</option>
                                    </select>

                                    @if ($errors->has('municipio_residencia'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('municipio_residencia') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group col-md-4 col-lg-4">
                                <label>{{ trans('labels.direccion_residencia') }}</label>
                                <div class="input-group input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-certificate"></i></span>
                                    </div>
                                    <input placeholder="{{ trans('labels.direccion_residencia') }}" id="direccion" type="text" class="form-control{{ $errors->has('direccion') ? ' is-invalid' : '' }}" name="direccion" value="{{ old('direccion') }}" required>

                                    @if ($errors->has('direccion'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('direccion') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6 col-lg-6">
                                <label>{{ trans('labels.email') }}</label>
                                <div class="input-group input-group-alternative mb-3">
                                    

                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                    </div>
                                    <input placeholder="{{ trans('labels.email') }}" id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required >

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group col-md-6 col-lg-6">
                                <label>{{ trans('labels.confirm_email') }}</label>
                                <div class="input-group input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                    </div>
                                    <input placeholder="{{ trans('labels.confirm_email') }}" id="email_verified" type="email" class="form-control{{ $errors->has('email_verified') ? ' is-invalid' : '' }}" name="email_verified" value="{{ old('email_verified') }}" required>

                                    @if ($errors->has('email_verified'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email_verified') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6 col-lg-6">
                                <label>{{ trans('labels.password') }}</label>
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                    </div>
                                    <input placeholder="{{ trans('labels.password') }}" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group col-md-6 col-lg-6">
                                <label>{{ trans('labels.confirm_password') }}</label>
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                    </div>
                                    <input placeholder="{{ trans('labels.confirm_password') }}" id="password_confirmation" type="password" class="form-control" name="password_confirmation" required>
                                    @if ($errors->has('password_confirmation'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary mt-4">{{trans('labels.regist')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    async function getMunicipios() {

        var id_departamento = document.getElementById('departamento_residencia').value;

        await fetch('{{ url("obtener_municipio") }}/' + id_departamento)
            .then(response => response.json())
            .then(data => {
                document.getElementById('municipio_residencia').innerHTML = "";
                for (const municipio of data) {
                    var opcion = document.createElement('option');
                    opcion.value = municipio.id;
                    opcion.innerHTML = municipio.nombre;
                    document.getElementById('municipio_residencia').appendChild(opcion);
                }
            });
    }

</script>
@endsection
