@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        <div class="row justify-content-between">
            <h2>Administración de Usuarios</h2>
            <x-exportador model="usuarios" />
        </div>
    </div>
    <div class="card-body">
        <form action="@if(isset($usuario)){{route('usuarios.update', $usuario->id)}}@else{{route('usuarios.store')}}@endif" method="POST">
            @csrf
            @if(isset($usuario))
                @method('PUT')
            @endif
        
            <div class="input-group">
                <div class="col-md-1">
                    <label class="col-form-label-sm">Usuario</label>
                </div>
                <div class="col-md-3">
                    <input type="email" class="form-control form-control-sm" name="email" placeholder="Usuario" value="@if(isset($usuario)){{$usuario->email}}@endif">
                </div>
                <div class="col-md-1">
                    <label class="col-form-label-sm">Contraseña</label>
                </div>
                <div class="col-md-3">
                    <input type="password" class="form-control form-control-sm" name="password" placeholder="Contraseña">
                </div>
                <div class="col-md-1">
                    <label class="col-form-label-sm">Rol</label>
                </div>  
                <div class="col-md-3">
                    <select name="rol_id" class="form-control form-control-sm">
                        @foreach ($listas['roles'] as $item)
                            <option value="{{ $item->id }}" @if(isset($usuario) && $usuario->rol_id == $item->id){{'selected'}}@endif>{{ $item->nombre }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-1">
                    <label class="col-form-label-sm">Estado</label>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <select name="estado" class="form-control form-control-sm">
                            <option value="1" @if(isset($usuario) && $usuario->estado == 1){{'selected'}}@endif>Activo</option>
                            <option value="2" @if(isset($usuario) && $usuario->estado == 2){{'selected'}}@endif>Inactivo</option>
                        </select>
                        <div class="input-group-append" id="button-addon4">
                            <button class="btn btn-primary btn-sm" type="submit"><i class="fa @if(isset($usuario)){{'fa-pencil-alt'}}@else{{'fa-save'}}@endif"></i> @if(isset($usuario)){{'Actualizar'}}@else{{'Guardar'}}@endif</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        
        <br>
        
        <div class="table-responsive">
            <table class="table table-striped" id="datos">
                <thead>
                    <th>Usuario</th>
                    <th>Rol</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </thead>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#datos').dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "method": "POST",
                "url": "{{ route('usuarios.get') }}",
                "data": {"_token" : document.getElementsByName("_token")[0].value}
            },
            "order": [[0, 'asc']],
            "columns": [
                {data: 'email', name: 'email'},
                {data: 'roles.nombre', name: 'roles.nombre', width: '100px'},
                {data: 'estado', name: 'estado', width:'50px'},
                {data: 'btn', name: 'btn', width:'200px'},
            ],
            "language": {
                "url" : "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            }
        });
    });

    function confirmacionEliminar(formulario) {
        swal({
            title: '¿Está seguro de eliminar el registro?',
            text: "Esta acción será irreversible",
            icon: 'warning',
            buttons: {
                confirm: {
                    text: "SI",
                    value: true,
                    visible: true,
                    className: "btn btn-success",
                    closeModal: true
                },
                cancel: {
                    text: "NO",
                    value: false,
                    visible: true,
                    className: "btn btn-danger",
                    closeModal: true,
                }
            }
        }).then((result) => {
            if (result) {
                document.getElementById("frmEliminar" + formulario.toString()).submit();
            }
        });
    }
  
</script>
@endsection