@extends('layouts.app')

@section('content')

<form action="@if(isset($servicio)){{ route('aseolimpieza.update', $servicio->id) }}@else{{ route('aseolimpieza.store') }}@endif" method="post">
@csrf
@if(isset($servicio))
    @method("PUT")
@endif
<div class="row">
    <div class="col-md-6">
        <label for="tipo_servicio" class="col-form-label-sm">Tipo de servicio</label>
        <select name="tipo_servicio" id="tipo_servicio" class="form-control form-control-sm" onchange="cambiarTipoServicio();">
            @foreach ($tiposervicios as $item)
                <option value="{{$item->id}}" @if(isset($servicio) && $servicio->tipo_servicio_id == $item->id){{'selected'}}@endif>{{$item->nombre}} ({{$item->tipo_moneda}} {{$item->precio_hora}}/H)</option>
            @endforeach
        </select>
        <label class="text-danger text-bold col-form-label-sm">* Aseo general desde 3 horas; Aseo general y lavandería desde 2 horas</label>
    </div>
    <div class="col-md-2">
        <label for="horas_servicio" class="col-form-label-sm">Horas servicio</label>
        <div class="input-group input-group-sm mb-3">
            <input type="number" name="horas_servicio" id="horas_servicio" class="form-control form-control-sm" aria-label="Horas del servicio" aria-describedby="precio_addon" min="2" value="2" onchange="calcularValor();" value="@if(isset($servicio)){{$servicio->horas_servicio}}@endif">
            <div class="input-group-append">
              <span class="input-group-text text-bold" id="precio_addon">$ 17.000</span>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <label for="fecha_servicio" class="col-form-label-sm">Fecha servicio</label>
        <div class="input-group input-group-sm">
            <input class="form-control" type="date" name="fecha_servicio" id="fecha_servicio" value="@if(isset($servicio)){{ date_format(date_create($servicio->fecha_servicio), 'Y-m-d') }}@endif">
            <input class="form-control" type="time" name="hora_servicio" id="hora_servicio" min="06:00:00" max="18:00:00" value="@if(isset($servicio)){{ date_format(date_create($servicio->fecha_servicio), 'H:i') }}@endif">
        </div>
    </div>
    <div class="col-md-4">
        <label for="direccion" class="col-form-label-sm">Dirección</label>
        <input type="text" name="direccion" id="direccion" class="form-control form-control-sm" value="@if(isset($servicio)){{ $servicio->direccion }}@else{{ $persona->direccion }}@endif">
    </div>
    <div class="col-md-4">
        <label for="telefono" class="col-form-label-sm">Teléfono</label>
        <input type="text" name="telefono" id="telefono" class="form-control form-control-sm" value="@if(isset($servicio)){{$servicio->telefono}}@else{{ $persona->telefono_celular }}@endif">
    </div>
    <div class="col-md-4">
        <label for="email" class="col-form-label-sm">Correo electrónico</label>
        <input type="email" name="email" id="email" class="form-control form-control-sm" value="@if(isset($servicio)){{$servicio->correo_electronico}}@else{{ auth()->user()->email }}@endif">
    </div>
    <div class="col-md-12">
        <label for="observaciones" class="col-form-label-sm">Observaciones</label>
        <textarea name="observaciones" id="observaciones" rows="5" class="form-control form-control-sm" placeholder="Escribe aquí cualquier dato adicional que sea de importancia">@if(isset($servicio)){{$servicio->observaciones}}@endif</textarea>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12 text-right">
        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-envelope"></i> Enviar solicitud</button>
    </div>
</div>
</form>

<script>
    $(document).ready(function() {
        Date.prototype.toDateInputValue = (function() {
            var local = new Date(this);
            local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
            return local.toJSON().slice(0,10);
        });

        // document.getElementById('fecha_servicio').value = new Date().toDateInputValue();
        document.getElementById('fecha_servicio').min = new Date().toDateInputValue();
    });

    function cambiarTipoServicio() {
        var tipo_servicio = document.getElementById('tipo_servicio').value;
        if(tipo_servicio == 1) { // ASEO GENERAL
            document.getElementById('horas_servicio').min = "2";
            document.getElementById('horas_servicio').value = "2";
            document.getElementById('precio_addon').innerText = "$ 17.000";
        } else if(tipo_servicio == 2) { // CON LAVANDERIA
            document.getElementById('horas_servicio').min = "3";
            document.getElementById('horas_servicio').value = "3";
            document.getElementById('precio_addon').innerText = "$ 33.000";
        }
    }

    function calcularValor(value) {
        var horas = document.getElementById('horas_servicio').value;
        var tipo_servicio = document.getElementById('tipo_servicio').value;
        if(tipo_servicio == 1) { // ASEO GENERAL
            document.getElementById('precio_addon').innerText = "$ " + (horas * 8500).toLocaleString(undefined, {minimumFractionDigits: 0});
        } else if(tipo_servicio == 2) { // CON LAVANDERIA
            document.getElementById('precio_addon').innerText = "$ " + (horas * 11000).toLocaleString(undefined, {minimumFractionDigits: 0});
        }
    }
</script>
@endsection