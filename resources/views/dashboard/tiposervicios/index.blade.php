@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        <div class="row justify-content-between">
            <h2>Administración de Tipos de Servicios</h2>
            <x-exportador model="tiposervicios" />
        </div>
    </div>
    <div class="card-body">
        <form action="@if(isset($tiposervicios)){{route('tiposervicios.update', $tiposervicios->id)}}@else{{route('tiposervicios.store')}}@endif" method="POST">
            @csrf
            @if(isset($tiposervicios))
                @method('PUT')
            @endif
            <div class="input-group">
                <label class="col-md-1 col-form-label-sm">Area desempeñar</label>
                <div class="col-md-3">
                    <select name="area_desempenar_id" id="area_desempenar_id" class="form-control form-control-sm">
                        @foreach ($listas['area_desempenar'] as $item)
                        <option value="{{$item->id}}" @if(isset($tiposervicios) && $tiposervicios->area_desempenar_id == $item->id){{'selected'}}@endif>{{$item->nombre}}</option>
                        @endforeach
                    </select>
                </div>
                <label class="col-md-1 col-form-label-sm">Nombre</label>
                <div class="col-md-3">
                    <input type="text" class="form-control form-control-sm" name="nombre" placeholder="Nombre" value="@if(isset($tiposervicios)){{$tiposervicios->nombre}}@endif">
                </div>
                <label class="col-md-1 col-form-label-sm">Tipo moneda</label>
                <div class="col-md-3">
                    <select name="tipo_moneda" id="tipo_moneda" class="form-control form-control-sm">
                        <option value="COP" @if(isset($tiposervicios) && $tiposervicios->tipo_moneda == 'COP'){{'selected'}}@endif>Peso Colombiano</option>
                        <option value="USD" @if(isset($tiposervicios) && $tiposervicios->tipo_moneda == 'USD'){{'selected'}}@endif>Dolar</option>
                        <option value="EUR" @if(isset($tiposervicios) && $tiposervicios->tipo_moneda == 'EUR'){{'selected'}}@endif>Euro</option>
                    </select>
                </div>
                <label class="col-md-1 col-form-label-sm">Precio hora</label>
                <div class="col-md-3">
                    <input type="text" class="form-control form-control-sm" name="precio_hora" placeholder="Precio hora" value="@if(isset($tiposervicios)){{$tiposervicios->precio_hora}}@endif">
                </div>
                <label class="col-md-1 col-form-label-sm">Estado</label>
                <div class="col-md-3">
                    <div class="input-group">
                        <select name="estado" class="form-control form-control-sm">
                            <option value="1" @if(isset($tiposervicios) && $tiposervicios->estado == 1){{'selected'}}@endif>Activo</option>
                            <option value="2" @if(isset($tiposervicios) && $tiposervicios->estado == 2){{'selected'}}@endif>Inactivo</option>
                        </select>
                        <div class="input-group-append" id="button-addon4">
                            <button class="btn btn-primary btn-sm" type="submit"><i class="fa @if(isset($tiposervicios)){{'fa-pencil-alt'}}@else{{'fa-save'}}@endif"></i> @if(isset($tiposervicios)){{'Actualizar'}}@else{{'Guardar'}}@endif</button>
                        </div>
                    </div>
                </div>
                
            </div>
        </form>
        
        <br>
        
        <div class="table-responsive">
            <table class="table table-striped" id="datos">
                <thead>
                    <th>Area desempeñar</th>
                    <th>Nombre</th>
                    <th>Tipo Moneda</th>
                    <th>Precio Hora</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </thead>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#datos').dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "method": "POST",
                "url": "{{ route('tiposervicios.get') }}",
                "data": {"_token" : document.getElementsByName("_token")[0].value}
            },
            "order": [[0, 'asc']],
            "columns": [
                {data: 'area_desempenar', name: 'area_desempenar'},
                {data: 'nombre', name: 'nombre'},
                {data: 'tipo_moneda', name: 'tipo_moneda'},
                {data: 'precio_hora', name: 'precio_hora'},
                {data: 'estado', name: 'estado', width:'50px'},
                {data: 'btn', name: 'btn', width:'200px'},
            ],
            "language": {
                "url" : "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            }
        });
    });

    function confirmacionEliminar(formulario) {
        swal({
            title: '¿Está seguro de eliminar el registro?',
            text: "Esta acción será irreversible",
            icon: 'warning',
            buttons: {
                confirm: {
                    text: "SI",
                    value: true,
                    visible: true,
                    className: "btn btn-success",
                    closeModal: true
                },
                cancel: {
                    text: "NO",
                    value: false,
                    visible: true,
                    className: "btn btn-danger",
                    closeModal: true,
                }
            }
        }).then((result) => {
            if (result) {
                document.getElementById("frmEliminar" + formulario.toString()).submit();
            }
        });
    }
  
</script>
@endsection