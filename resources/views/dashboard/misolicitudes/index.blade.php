@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        <div class="row justify-content-between">
            <h2>Mis solicitudes</h2>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-4">
                <label for="tipo_servicio" class="col-form-label-sm">Tipo de servicio</label>
                <div class="input-group">
                    <select name="tipo_servicio" id="tipo_servicio" class="form-control form-control-sm">
                        @foreach ($listas['area_desempenar'] as $item)
                            <option value="{{$item->id}}">{{$item->nombre}}</option>
                        @endforeach
                    </select>
                    <div class="input-group-append" id="button-addon4">
                        <button type="button" class="btn btn-sm btn-primary" onclick="ConsultarDatos(document.getElementById('tipo_servicio').value);"><i class="fa fa-search"></i> Consultar</button>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <table id="datos"></table>
    </div>
</div>

<script>
    function ConsultarDatos(area_desempenar) {
        switch(area_desempenar) {
            case "1":
                $('#datos').dataTable({
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "method": "POST",
                        "url": "{{ route('aseolimpieza.data') }}",
                        "data": {"_token" : document.getElementsByName("_token")[0].value}
                    },
                    "order": [[0, 'asc']],
                    "columns": [
                        {title: "Fecha", data: 'fecha_servicio'},
                        {title: "Nombre", data: 'nombre'},
                        {title: "No. horas", data: 'horas_servicio'},
                        {title: "Dirección", data: 'direccion'},
                        {title: "Teléfono", data: 'telefono'},
                        {title: "Email", data: 'correo_electronico'},
                        {title: "Valor hora", data: 'valor_hora'},
                        {title: "Total", data: 'total'},
                        {title: "Acciones", data: 'btn'},
                    ],
                    "language": {
                        "url" : "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                    }
                });
                break;
        }
    }
</script>

@endsection