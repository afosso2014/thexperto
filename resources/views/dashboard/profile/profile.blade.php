@extends('layouts.app')

@section('content')
<div class="header pb-8 d-flex align-items-center"
     style="min-height: 600px; background-image: url({{ asset('assets/img/brand/logo_original.png') }}); background-size: cover; background-position: center top;">
    <!-- Mask -->
    <span class="mask bg-gradient-default opacity-8"></span>
    <!-- Header container -->
    <div class="container-fluid d-flex align-items-center">
        <div class="row">
            <div class="col-lg-7 col-md-10">
                <h1 class="display-2 text-white mt--7">Hola {{ Auth()->user()->name }}</h1>
                <p class="text-white mt--3">Esta es tu página de perfil. Usted puede ver sus datos personales y las diferentes solicitudes realizadas</p>
                <!--<a href="#!" class="btn btn-info">Editar Perfil</a>-->
            </div>
        </div>
    </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--9">
    <div class="row">
        <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
            <div class="card card-profile shadow">
                <div class="row justify-content-center">
                    <div class="col-lg-3 order-lg-2">
                        <div class="card-profile-image">
                            <a href="#">
                                <img src="../assets/img/theme/team-4-800x800.jpg" class="rounded-circle">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                    <div class="d-flex justify-content-between">
                        <a href="#" class="btn btn-sm btn-info mr-4">{{ __('Connect') }}</a>
                        <a href="#" class="btn btn-sm btn-default float-right">{{ __('Message') }}</a>
                    </div>
                </div>
                <div class="card-body pt-0 pt-md-4">
                    <div class="row">
                        <div class="col">
                            <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                                <div>
                                    <span class="heading">0</span>
                                    <span class="description">Solicitudes</span>
                                </div>
                                <div>
                                    <span class="heading">0</span>
                                    <span class="description">Aceptadas</span>
                                </div>
                                <div>
                                    <span class="heading">0</span>
                                    <span class="description">Rechazadas</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <h3>
                            {{ $persona->nombre . " " . $persona->apellido }}<span class="font-weight-light"></span>
                        </h3>
                        <div class="h5 font-weight-300">
                            <i class="ni location_pin mr-2"></i>{{ $persona->nombreMunicipio }}
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-8 order-xl-1">
            <div class="card bg-secondary shadow">
                <div class="card-header bg-white border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{ __("Mi perfil") }}</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{ route('profile.update', $persona->id) }}" class="btn btn-sm btn-primary"
                               onclick="event.preventDefault();
                                document.getElementById('infoBasica').submit();">
                                <i class="fa fa-save"></i> Guardar
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form id="infoBasica" method="POST" action="{{ route('profile.update', $persona->id) }}">
                        {!! method_field('PUT') !!}
                        @csrf
                        <h6 class="heading-small text-muted mb-4">Información de usuario</h6>
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="nombre">Nombres</label>
                                        <input type="text" id="nombre" name="nombre" class="form-control form-control-alternative" placeholder="Nombres" value="{{ $persona->nombres }}" disabled>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="apellido">Apellidos</label>
                                        <input type="email" id="apellido" name="apellido" class="form-control form-control-alternative" placeholder="Apellidos" value="{{ $persona->apellidos }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="tipo_documento">Tipo Documento</label>
                                        <select name="tipo_documento" id="tipo_documento" class="form-control form-control-alternative">
                                            <option value="-1">--Seleccione un tipo de documento--</option>
                                            @foreach($tipoDocumentos as $tipoDocumento)
                                                <option value="{{$tipoDocumento->id}}" @if($tipoDocumento->id == $persona->tipo_documento_id){{'selected'}}@endif>{{$tipoDocumento->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="numero_documento">Numero Documento</label>
                                        <input type="text" id="numero_documento" name="numero_documento" class="form-control form-control-alternative" placeholder="Número de documento" value="{{ $persona->numero_documento }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="my-4" />
                        <!-- Address -->
                        <h6 class="heading-small text-muted mb-4">Información de contacto</h6>
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="direccion">Dirección</label>
                                        <input id="direccion" name="direccion" class="form-control form-control-alternative" placeholder="Dirección de residencia" value="{{ $persona->direccion }}" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="telefono_fijo" class="form-control-label">Teléfono Fijo</label>
                                        <input type="text" id="telefono_fijo" name="telefono_fijo" class="form-control form-control-alternative" placeholder="Teléfono fijo" value="{{ $persona->telefono_fijo }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="telefono_celular" class="form-control-label">Teléfono Celular</label>
                                        <input type="text" id="telefono_celular" name="telefono_celular" class="form-control form-control-alternative" placeholder="Teléfono celular" value="{{ $persona->telefono_celular }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="pais">Pais</label>
                                        <select name="pais" id="pais" class="form-control form-control-alternative" disabled>
                                            <option value="-1">--Seleccione una opción--</option>
                                            @foreach($paises as $pais)
                                                <option value="{{$pais->id}}" @if($pais->id == $persona->pais_id){{'selected'}}@endif>{{$pais->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="departamento">Departamento</label>
                                        <select name="departamento" id="departamento" class="form-control" disabled>
                                            <option value="-1">--Seleccione una opción--</option>
                                            @foreach($departamentos as $departamento)
                                                <option value="{{$departamento->id}}" @if($departamento->id == $persona->departamento_id){{'selected'}}@endif>{{$departamento->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="municipio">Municipio</label>
                                        <select name="municipio" id="municipio" class="form-control" disabled>
                                            <option value="-1">--Seleccione una opción--</option>
                                            @foreach($municipios as $municipio)
                                                <option value="{{$municipio->id}}" @if($municipio->id == $persona->municipio_id){{'selected'}}@endif>{{$municipio->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</div>
@endsection