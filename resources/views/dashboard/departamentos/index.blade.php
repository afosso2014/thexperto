@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        <div class="row justify-content-between">
            <h2>Administración de Departamentos</h2>
            <x-exportador model="departamentos" />
        </div>
    </div>
    <div class="card-body">
        <form action="@if(isset($departamento)){{route('departamentos.update', $departamento->id)}}@else{{route('departamentos.store')}}@endif" method="POST">
            @csrf
            @if(isset($departamento))
                @method('PUT')
            @endif
            <div class="input-group">
                <label class="col-md-1 col-form-label-sm">Pais</label>
                <div class="col-md-3">
                    <select name="pais_id" class="form-control form-control-sm">
                        @foreach ($listas['paises'] as $item)
                        <option value="{{$item->id}}" @if(isset($departamento) && $departamento->pais_id == $item->id){{'selected'}}@endif>{{$item->nombre}}</option>
                        @endforeach
                    </select>
                </div>
                <label class="col-md-2 col-form-label-sm">Codigo DANE</label>
                <div class="col-md-2">
                    <input type="text" class="form-control form-control-sm" name="codigoDane" placeholder="Código DANE" value="@if(isset($departamento)){{$departamento->codigoDane}}@endif">
                </div>
                <label class="col-md-1 col-form-label-sm">Nombre</label>
                <div class="col-md-3">
                    <input type="text" class="form-control form-control-sm" name="nombre" placeholder="Nombre" value="@if(isset($departamento)){{$departamento->nombre}}@endif">
                </div>
                <label class="col-md-1 col-form-label-sm">Estado</label>
                <div class="col-md-3">
                    <div class="input-group">
                        <select name="estado" class="form-control form-control-sm">
                            <option value="1" @if(isset($departamento) && $departamento->estado == 1){{'selected'}}@endif>Activo</option>
                            <option value="2" @if(isset($departamento) && $departamento->estado == 2){{'selected'}}@endif>Inactivo</option>
                        </select>
                        <div class="input-group-append" id="button-addon4">
                            <button class="btn btn-primary btn-sm" type="submit"><i class="fa @if(isset($departamento)){{'fa-pencil-alt'}}@else{{'fa-save'}}@endif"></i> @if(isset($departamento)){{'Actualizar'}}@else{{'Guardar'}}@endif</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        
        <br>
        
        <div class="table-responsive">
            <table class="table table-striped" id="datos">
                <thead>
                    <th>Pais</th>
                    <th>Código DANE</th>
                    <th>Nombre</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </thead>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#datos').dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "method": "POST",
                "url": "{{ route('departamentos.get') }}",
                "data": {"_token" : document.getElementsByName("_token")[0].value}
            },
            "order": [[0, 'asc']],
            "columns": [
                {data: 'pais', name: 'pais', width: '50px'},
                {data: 'codigoDane', name: 'codigoDane', width: '50px'},
                {data: 'nombre', name: 'nombre'},
                {data: 'estado', name: 'estado', width:'50px'},
                {data: 'btn', name: 'btn', width:'200px'},
            ],
            "language": {
                "url" : "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            }
        });
    });

    function confirmacionEliminar(formulario) {
        swal({
            title: '¿Está seguro de eliminar el registro?',
            text: "Esta acción será irreversible",
            icon: 'warning',
            buttons: {
                confirm: {
                    text: "SI",
                    value: true,
                    visible: true,
                    className: "btn btn-success",
                    closeModal: true
                },
                cancel: {
                    text: "NO",
                    value: false,
                    visible: true,
                    className: "btn btn-danger",
                    closeModal: true,
                }
            }
        }).then((result) => {
            if (result) {
                document.getElementById("frmEliminar" + formulario.toString()).submit();
            }
        });
    }
  
</script>
@endsection