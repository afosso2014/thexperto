@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        <div class="row justify-content-between">
            <h2>Administración de Bancos</h2>
            <x-exportador model="bancos" />
        </div>
    </div>
    <div class="card-body">
        <form action="@if(isset($bancos)){{route('bancos.update', $bancos->id)}}@else{{route('bancos.store')}}@endif" method="POST">
            @csrf
            @if(isset($bancos))
                @method('PUT')
            @endif
            <div class="input-group">
                <label class="col-md-1 col-form-label-sm">Código</label>
                <div class="col-md-3">
                    <input type="text" class="form-control form-control-sm" name="codigo" placeholder="Código" value="@if(isset($bancos)){{$bancos->codigo}}@endif">
                </div>
                <label class="col-md-1 col-form-label-sm">Nombre</label>
                <div class="col-md-3">
                    <input type="text" class="form-control form-control-sm" name="nombre" placeholder="Nombre" value="@if(isset($bancos)){{$bancos->nombre}}@endif">
                </div>
                <label class="col-md-1 col-form-label-sm">Estado</label>
                <div class="col-md-3">
                    <div class="input-group">
                        <select name="estado" class="form-control form-control-sm">
                            <option value="1" @if(isset($bancos) && $bancos->estado == 1){{'selected'}}@endif>Activo</option>
                            <option value="2" @if(isset($bancos) && $bancos->estado == 2){{'selected'}}@endif>Inactivo</option>
                        </select>
                        <div class="input-group-append" id="button-addon4">
                            <button class="btn btn-primary btn-sm" type="submit"><i class="fa @if(isset($bancos)){{'fa-pencil-alt'}}@else{{'fa-save'}}@endif"></i> @if(isset($bancos)){{'Actualizar'}}@else{{'Guardar'}}@endif</button>
                        </div>
                    </div>
                </div>
                
            </div>
        </form>
        
        <br>
        
        <div class="table-responsive">
            <table class="table table-striped" id="datos">
                <thead>
                    <th>Código</th>
                    <th>Nombre</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </thead>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#datos').dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "method": "POST",
                "url": "{{ route('bancos.get') }}",
                "data": {"_token" : document.getElementsByName("_token")[0].value}
            },
            "order": [[0, 'asc']],
            "columns": [
                {data: 'codigo', name: 'codigo'},
                {data: 'nombre', name: 'nombre'},
                {data: 'estado', name: 'estado', width:'50px'},
                {data: 'btn', name: 'btn', width:'200px'},
            ],
            "language": {
                "url" : "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            }
        });
    });

    function confirmacionEliminar(formulario) {
        swal({
            title: '¿Está seguro de eliminar el registro?',
            text: "Esta acción será irreversible",
            icon: 'warning',
            buttons: {
                confirm: {
                    text: "SI",
                    value: true,
                    visible: true,
                    className: "btn btn-success",
                    closeModal: true
                },
                cancel: {
                    text: "NO",
                    value: false,
                    visible: true,
                    className: "btn btn-danger",
                    closeModal: true,
                }
            }
        }).then((result) => {
            if (result) {
                document.getElementById("frmEliminar" + formulario.toString()).submit();
            }
        });
    }
  
</script>
@endsection