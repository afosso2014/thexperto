@extends('layouts.app')

@section('content')
@if(auth()->user()->rol_id == \App\Rol::EXPERTO)

<x-solicitudes-clientes />

@elseif(auth()->user()->rol_id == \App\Rol::USUARIO)
    <h2>Solicita cualquiera de los siguientes servicios</h2>
    <div class="row justify-content-center">
        <x-servicio nombreServicio="Aseo y limpieza" colorFondo="bg-blue" icono="fa fa-broom" ruta="aseolimpieza.index" />
        <x-servicio nombreServicio="Jardinería y fumigación" colorFondo="bg-green" icono="fa fa-tree" ruta="aseolimpieza.index" />
        <x-servicio nombreServicio="Mto. y reparaciones locativas" colorFondo="bg-yellow" icono="fa fa-brush" ruta="aseolimpieza.index" />
    </div>
@elseif(auth()->user()->rol_id == \App\Rol::ADMINISTRADOR)
<h1>DASHBOARD ADMINISTRADOR</h1>
@endif

@endsection
