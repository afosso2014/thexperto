@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        <div class="row justify-content-between">
            <h2>Administración de Formularios</h2>
            <x-exportador model="formularios" />
        </div>
    </div>
    <div class="card-body">
        <form action="@if(isset($formulario)){{route('formularios.update', $formulario->id)}}@else{{route('formularios.store')}}@endif" method="POST">
            @csrf
            @if(isset($formulario))
                @method('PUT')
            @endif
            <div class="input-group">
                <label class="col-md-1 col-form-label-sm">Nombre</label>
                <div class="col-md-3">
                    <input type="text" class="form-control form-control-sm" name="nombre" placeholder="Nombre" value="@if(isset($formulario)){{$formulario->nombre}}@endif">
                </div>
                <label class="col-md-1 col-form-label-sm">Icono</label>
                <div class="col-md-3">
                    <input type="text" class="form-control form-control-sm" name="icono" placeholder="Icono" value="@if(isset($formulario)){{$formulario->icono}}@endif">
                </div>
                <label class="col-md-1 col-form-label-sm">URL</label>
                <div class="col-md-3">
                    <input type="text" class="form-control form-control-sm" name="url" placeholder="URL" value="@if(isset($formulario)){{$formulario->url}}@endif">
                </div>
                <label class="col-md-1 col-form-label-sm">Estado</label>
                <div class="col-md-3">
                    <div class="input-group">
                        <select name="estado" class="form-control form-control-sm">
                            <option value="1" @if(isset($formulario) && $formulario->estado == 1){{'selected'}}@endif>Activo</option>
                            <option value="2" @if(isset($formulario) && $formulario->estado == 2){{'selected'}}@endif>Inactivo</option>
                        </select>
                        <div class="input-group-append" id="button-addon4">
                            <button class="btn btn-primary btn-sm" type="submit"><i class="fa @if(isset($formulario)){{'fa-pencil-alt'}}@else{{'fa-save'}}@endif"></i> @if(isset($formulario)){{'Actualizar'}}@else{{'Guardar'}}@endif</button>
                        </div>
                    </div>
                </div>
                
            </div>
        </form>
        
        <br>
        
        <div class="table-responsive">
            <table class="table table-striped" id="datos">
                <thead>
                    <th>Nombre</th>
                    <th>Icono</th>
                    <th>URL</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </thead>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#datos').dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "method": "POST",
                "url": "{{ route('formularios.get') }}",
                "data": {"_token" : document.getElementsByName("_token")[0].value}
            },
            "order": [[0, 'asc']],
            "columns": [
                {data: 'nombre', name: 'nombre'},
                {data: 'icono', name: 'icono'},
                {data: 'url', name: 'url'},
                {data: 'estado', name: 'estado', width:'50px'},
                {data: 'btn', name: 'btn', width:'200px'},
            ],
            "language": {
                "url" : "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            }
        });
    });

    function confirmacionEliminar(formulario) {
        swal({
            title: '¿Está seguro de eliminar el registro?',
            text: "Esta acción será irreversible",
            icon: 'warning',
            buttons: {
                confirm: {
                    text: "SI",
                    value: true,
                    visible: true,
                    className: "btn btn-success",
                    closeModal: true
                },
                cancel: {
                    text: "NO",
                    value: false,
                    visible: true,
                    className: "btn btn-danger",
                    closeModal: true,
                }
            }
        }).then((result) => {
            if (result) {
                document.getElementById("frmEliminar" + formulario.toString()).submit();
            }
        });
    }
  
</script>
@endsection