@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        <div class="row justify-content-between">
            <h2>Asignar Formularios a Roles</h2>
            <x-exportador model="rolesformularios" />
        </div>
    </div>
    <div class="card-body">
        <form action="{{route('rolesformularios.store')}}" method="POST">
            @csrf
            <div class="input-group">
                <label class="col-form-label-sm">Rol</label>
                <div class="col-md-3">
                    <select name="rol" class="form-control form-control-sm">
                        @foreach ($listas['roles'] as $item)
                            <option value="{{$item->id}}">{{$item->nombre}}</option>
                        @endforeach
                    </select>
                </div>
                <label class="col-form-label-sm">Formulario</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <select name="formulario" class="form-control form-control-sm">
                            @foreach ($listas['formularios'] as $item)
                                <option value="{{$item->id}}">{{$item->nombre}}</option>
                            @endforeach
                        </select>
                        <div class="input-group-append" id="button-addon4">
                            <button class="btn btn-primary btn-sm" type="submit"><i class="fa @if(isset($rol)){{'fa-pencil-alt'}}@else{{'fa-save'}}@endif"></i> @if(isset($rol)){{'Actualizar'}}@else{{'Guardar'}}@endif</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        
        <br>
        
        <div class="table-responsive">
            <table class="table table-striped" id="datos">
                <thead>
                    <th>Rol</th>
                    <th>Formulario</th>
                    <th>Acciones</th>
                </thead>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#datos').dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "method": "POST",
                "url": "{{ route('rolesformularios.get') }}",
                "data": {"_token" : document.getElementsByName("_token")[0].value}
            },
            "order": [[0, 'asc']],
            "columns": [
                {data: 'rol.nombre', name: 'rol.nombre'},
                {data: 'formulario.nombre', name: 'formulario.nombre'},
                {data: 'btn', name: 'btn', width:'200px'},
            ],
            "language": {
                "url" : "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            }
        });
    });

    function confirmacionEliminar(formulario) {
        swal({
            title: '¿Está seguro de eliminar el registro?',
            text: "Esta acción será irreversible",
            icon: 'warning',
            buttons: {
                confirm: {
                    text: "SI",
                    value: true,
                    visible: true,
                    className: "btn btn-success",
                    closeModal: true
                },
                cancel: {
                    text: "NO",
                    value: false,
                    visible: true,
                    className: "btn btn-danger",
                    closeModal: true,
                }
            }
        }).then((result) => {
            if (result) {
                document.getElementById("frmEliminar" + formulario.toString()).submit();
            }
        });
    }
  
</script>
@endsection