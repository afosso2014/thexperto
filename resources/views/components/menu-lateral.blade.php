<li class="nav-item">
    <a class="nav-link" href="/dashboard">
        <i class="fa fa-home"></i> Inicio
    </a>
</li>    

@foreach ($formularios as $item)
    <li class="nav-item">
        <a class="nav-link" href="/dashboard/{{ $item->url }}/index">
            <i class="@if($item->icono != null){{$item->icono}}@else{{'fa fa-bars'}}@endif"></i> {{ $item->nombre }}
        </a>
    </li>    
@endforeach