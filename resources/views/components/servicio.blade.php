<form action="{{ route($ruta) }}" method="get">
    <div style="width: 18rem;">
        <div class="card card-stats">
            <div class="card-body {{ $colorFondo }}">
                <div class="row">
                    <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-0 text-white">{{ $nombreServicio }}</h5>
                    </div>
                    <div class="col-auto">
                        <div class="icon icon-shape text-white rounded-circle shadow">
                            <i class="{{ $icono }}"></i>
                        </div>
                    </div>
                </div>
                <p class="mt-3 mb-0 text-sm">
                    <button type="submit" class="btn btn-white btn-outline-neutral">
                        <span class="text-nowrap">Solicitar</span>
                    </button>
                </p>
            </div>
        </div>
    </div>
</form>