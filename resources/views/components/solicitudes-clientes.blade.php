
<div class="card">
    <div class="card-header">
        <h2 class="text-muted">Solicitudes de clientes</h2>
    </div>
    <div class="card-body">
        <table class="table" id="datos"></table>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#datos').dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "method": "POST",
                "url": "{{ route('paises.get') }}",
                "data": {"_token" : document.getElementsByName("_token")[0].value}
            },
            "order": [[0, 'asc']],
            "columns": [
                {data: 'codigoDane', name: 'codigoDane', width: '50px'},
                {data: 'nombre', name: 'nombre'},
                {data: 'estado', name: 'estado', width:'50px'},
                {data: 'btn', name: 'btn', width:'200px'},
            ],
            "language": {
                "url" : "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            }
        });
    });
</script>