<div>
    <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
        <a href="{{ route('dashboard.export', ['excel', $model]) }}" class="btn btn-success" title="Descargar Excel" target="_blank"><i class="fa fa-file-excel"></i></a>
        <a href="{{ route('dashboard.export', ['csv', $model]) }}" class="btn btn-secondary" title="Descargar CSV" target="_blank"><i class="fa fa-file-alt"></i></a>
        <a href="{{ route('dashboard.export', ['pdf', $model]) }}" class="btn btn-danger" title="Descargar PDF" target="_blank"><i class="fa fa-file-pdf"></i></a>
    </div>
</div>