<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets') }}/vendor/nucleo/css/nucleo.css" rel="stylesheet">
    <link href="{{ asset('assets') }}/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link type="text/css" href="{{ asset('assets') }}/css/argon.css?v=1.0.0" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.css"/>
    <link rel="stylesheet" href="{{ asset('assets') }}/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <script src="{{ asset('assets') }}/vendor/jquery/dist/jquery.min.js"></script>
    <script src="{{ asset('assets') }}/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    @stack('js')
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('assets') }}/js/argon.js?v=1.0.0"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.js"></script>
    <script src="{{ asset('assets') }}/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
</head>
<body @if(Auth::guest()){!! 'Class="bg-default"' !!}@endif>
    @include('sweet::alert')

    @if(Auth::guest())
        <div class="main-content">
            @include('partials.navigations.guest')
            @yield('content')
            @include('partials.navigations.footer')
        </div>
    @else
        @include('partials.navigation')
        <div class="main-content">
            @include('partials.navigations.topbar')
            <div class="container-fluid mt-3">
                @yield('content')
                @include('partials.navigations.footer')
            </div>
        </div>
    @endif
</body>
</html>
