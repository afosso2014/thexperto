<footer class="py-5">
    <div class="container">
        <div class="row align-items-center justify-content-xl-between">
            <div class="col-xl-6">
                <div class="copyright text-center text-xl-left text-muted">
                    {{-- &copy; 2018 - <?php echo date('Y'); ?> <a href="https://www.thexperto.com" class="font-weight-bold ml-1" target="_blank">ThExperto</a> --}}
                </div>
            </div>
            <div class="col-xl-6">
                <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                    <li class="nav-item">
                        {{-- <a href="https://www.thexperto.com" class="nav-link" target="_blank">ThExperto</a> --}}
                    </li>
                    <li class="nav-item">
                        <a href="https://thexperto.com/nosotros/" class="nav-link" target="_blank">Nosotros</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>