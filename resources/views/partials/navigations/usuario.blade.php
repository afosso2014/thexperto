<li class="nav-item">
    <a class="nav-link active" href="{{ route('home') }}">
        <i class="ni ni-shop text-primary"></i> {{ __('Home') }}
    </a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{ route('profile.index') }}">
        <i class="fa fa-user-cog text-primary"></i> {{ __('Perfil') }}
    </a>
</li>
<li class="nav-item">
    <a class="nav-link" href="">
        <i class="fa fa-chalkboard-teacher text-primary"></i> {{ __('Realizar Solicitud') }}
    </a>
</li>