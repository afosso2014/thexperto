<li class="nav-item">
    <a class="nav-link" href="#">
        <i class="ni ni-settings-gear-65 text-primary"></i> {{ __('Banco') }}
    </a>
</li>
<li class="nav-item">
    <a class="nav-link" href="#">
        <i class="ni ni-settings-gear-65 text-primary"></i> {{ __('Tipo_Doc') }}
    </a>
</li>
<li class="nav-item">
    <a class="nav-link" href="#">
        <i class="ni ni-settings-gear-65 text-primary"></i> {{ __('Tipo Cuenta') }}
    </a>
</li>
<li class="nav-item">
    <a class="nav-link" href="#">
        <i class="ni ni-settings-gear-65 text-primary"></i> {{ __('Pais') }}
    </a>
</li>
<li class="nav-item">
    <a class="nav-link" href="#">
        <i class="ni ni-settings-gear-65 text-primary"></i> {{ __('Departamento') }}
    </a>
</li>
<li class="nav-item">
    <a class="nav-link" href="#">
        <i class="ni ni-settings-gear-65 text-primary"></i> {{ __('Municipio') }}
    </a>
</li>