<li class="nav-item">
    <a class="nav-link active" href="{{ route('home') }}">
        <i class="ni ni-shop text-primary"></i> {{ __('Home') }}
    </a>
</li>