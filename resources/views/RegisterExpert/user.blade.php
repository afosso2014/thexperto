
<div class="row">
    <div class="form-group col-md-6 col-lg-6">
        <label>{{ trans('labels.email') }}</label>
        <div class="input-group input-group-alternative mb-3">
            

            <div class="input-group-prepend">
                <span class="input-group-text"><i class="ni ni-email-83"></i></span>
            </div>
            <input placeholder="{{ trans('labels.email') }}" id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required >

            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-6 col-lg-6">
        <label>{{ trans('labels.confirm_email') }}</label>
        <div class="input-group input-group-alternative mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="ni ni-email-83"></i></span>
            </div>
            <input placeholder="{{ trans('labels.confirm_email') }}" id="email_verified" type="email" class="form-control{{ $errors->has('email_verified') ? ' is-invalid' : '' }}" name="email_verified" value="{{ old('email_verified') }}" required>

            @if ($errors->has('email_verified'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email_verified') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 col-lg-6">
        <label>{{ trans('labels.password') }}</label>
        <div class="input-group input-group-alternative">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
            </div>
            <input placeholder="{{ trans('labels.password') }}" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group col-md-6 col-lg-6">
        <label>{{ trans('labels.confirm_password') }}</label>
        <div class="input-group input-group-alternative">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
            </div>
            <input placeholder="{{ trans('labels.confirm_password') }}" id="password_confirmation" type="password" class="form-control" name="password_confirmation" required>
            @if ($errors->has('password_confirmation'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>




