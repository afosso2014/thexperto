<div class="row">
    <div class="form-group col-md-6 col-lg-6">
        <label>{{ trans('labels.area_desempenar') }}</label>
        <div class="input-group input-group-alternative mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-certificate"></i></span>
            </div>
            <select name="area_desempenar" id="area_desempenar" class="form-control{{ $errors->has('area_desempenar') ? ' is-invalid' : '' }}">
                @foreach($area_desempenar as $dato)
                    <option value="{{ $dato->id }}">{{ $dato->nombre }}</option>
                @endforeach
            </select>

            @if ($errors->has('area_desempenar'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('area_desempenar') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-6 col-lg-6">
        <label>{{ trans('labels.nivel_academico') }}</label>
        <div class="input-group input-group-alternative mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-certificate"></i></span>
            </div>
            <select name="nivel_academico" id="nivel_academico" class="form-control{{ $errors->has('nivel_academico') ? ' is-invalid' : '' }}">
                @foreach($nivel_academico as $dato)
                    <option value="{{ $dato->id }}">{{ $dato->nombre }}</option>
                @endforeach
            </select>

            @if ($errors->has('nivel_academico'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('nivel_academico') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>