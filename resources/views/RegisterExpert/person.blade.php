<div class="row">
    <div class="form-group col-md-6 col-lg-6">
        <label>{{ trans('labels.type_document') }}</label>
        <div class="input-group input-group-alternative mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-certificate"></i></span>
            </div>
            <select name="tipoDocumento" id="tipoDocumento" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}">
                @foreach($tiposDocumentos as $dato)
                    {!! "<option value='$dato->id'>$dato->nombre</option>" !!}
                @endforeach
            </select>

            @if ($errors->has('tipoDocumento'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('tipoDocumento') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-6 col-lg-6">
        <label>{{ trans('labels.number_document') }}</label>
        <div class="input-group input-group-alternative mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-certificate"></i></span>
            </div>
            <input placeholder="{{ trans('labels.number_document') }}" id="numero_identificacion" type="number" class="form-control{{ $errors->has('numero_identificacion') ? ' is-invalid' : '' }}" name="numero_identificacion" value="{{ old('numero_identificacion') }}" required>

            @if ($errors->has('numero_identificacion'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('numero_identificacion') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 col-lg-6">
        <label>{{ trans('labels.general.names') }}</label>
        <div class="input-group input-group-alternative mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-certificate"></i></span>
            </div>
            <input placeholder="{{ trans('labels.general.names') }}" id="nombres" type="text" class="form-control{{ $errors->has('nombres') ? ' is-invalid' : '' }}" name="nombres" value="{{ old('nombres') }}" required>

            @if ($errors->has('nombres'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('nombres') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-6 col-lg-6">
        <label>{{ trans('labels.general.last_names') }}</label>
        <div class="input-group input-group-alternative mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-certificate"></i></span>
            </div>
            <input placeholder="{{ trans('labels.general.last_names') }}" id="apellidos" type="text" class="form-control{{ $errors->has('apellidos') ? ' is-invalid' : '' }}" name="apellidos" value="{{ old('apellidos') }}" required>

            @if ($errors->has('apellidos'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('apellidos') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4 col-lg-4">
        <label>{{ trans('labels.date_born') }}</label>
        <div class="input-group input-group-alternative mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-certificate"></i></span>
            </div>
            <input placeholder="{{__('Fecha de Nacimiento')}}" id="nacimiento" type="date" class="form-control{{ $errors->has('nacimiento') ? ' is-invalid' : '' }}" name="nacimiento" value="{{ old('nacimiento') }}" required>

            @if ($errors->has('nacimiento'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('nacimiento') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-4 col-lg-4">
        <label>{{ trans('labels.genre') }}</label>
        <div class="input-group input-group-alternative mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-certificate"></i></span>
            </div>
            <select name="genero" id="genero" class="form-control{{ $errors->has('genero') ? ' is-invalid' : '' }}">
                <option value="M">Masculino</option>
                <option value="F">Femenino</option>
                <option value="O">Otro</option>
            </select>

            @if ($errors->has('genero'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('genero') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-4 col-lg-4">
        <label>{{ trans('labels.grupo_sanguineo') }}</label>
        <div class="input-group input-group-alternative mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-certificate"></i></span>
            </div>
            <select name="grupo_sanguineo" id="grupo_sanguineo" class="form-control{{ $errors->has('grupo_sanguineo') ? ' is-invalid' : '' }}">
                <option value="O+">O+</option>
                <option value="A+">A+</option>
                <option value="B+">B+</option>
                <option value="AB+">AB+</option>
                <option value="O-">O-</option>
                <option value="A-">A-</option>
                <option value="B-">B-</option>
                <option value="AB-">AB-</option>
            </select>

            @if ($errors->has('grupo_sanguineo'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('grupo_sanguineo') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4 col-lg-4">
        <label>{{ trans('labels.language') }}</label>
        <div class="input-group input-group-alternative mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-certificate"></i></span>
            </div>
            <select name="idioma" id="idioma" class="form-control{{ $errors->has('idioma') ? ' is-invalid' : '' }}">
                <option value="E">Español</option>
                <option value="I">Ingles</option>
                <option value="P">Portugues</option>
            </select>

            @if ($errors->has('idioma'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('idioma') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-4 col-lg-4">
        <label>{{ trans('labels.telefono_fijo') }}</label>
        <div class="input-group input-group-alternative mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-certificate"></i></span>
            </div>
            <input placeholder="{{ trans('labels.telefono_fijo') }}" id="telefono_fijo" type="number" class="form-control{{ $errors->has('telefono_fijo') ? ' is-invalid' : '' }}" name="telefono_fijo" value="{{ old('telefono_fijo') }}">

            @if ($errors->has('telefono_fijo'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('telefono_fijo') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-4 col-lg-4">
        <label>{{ trans('labels.telefono_celular') }}</label>
        <div class="input-group input-group-alternative mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-certificate"></i></span>
            </div>
            <input placeholder="{{ trans('labels.telefono_celular') }}" id="telefono_celular" type="number" class="form-control{{ $errors->has('telefono_celular') ? ' is-invalid' : '' }}" name="telefono_celular" value="{{ old('telefono_celular') }}" required>

            @if ($errors->has('telefono_celular'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('telefono_celular') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4 col-lg-4">
        <label>{{ trans('labels.department') }} Residencia</label>
        <div class="input-group input-group-alternative mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-certificate"></i></span>
            </div>
            <select name="departamento_residencia" id="departamento_residencia" class="form-control{{ $errors->has('departamento_residencia') ? ' is-invalid' : '' }}" onchange="getMunicipios();">
                <option value='-1'>--Seleccione un departamento--</option>
                @foreach($departamentos as $item)
                    {!! "<option value='$item->id'>$item->nombre</option>" !!}
                @endforeach
            </select>

            @if ($errors->has('departamento_residencia'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('departamento_residencia') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-4 col-lg-4">
        <label>{{ trans('labels.municipio') }} Residencia</label>
        <div class="input-group input-group-alternative mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-certificate"></i></span>
            </div>
            <select name="municipio_residencia" id="municipio_residencia" class="form-control{{ $errors->has('municipio_residencia') ? ' is-invalid' : '' }}">
                <option value="-1">--Seleccione su municipio de residencia--</option>
            </select>

            @if ($errors->has('municipio_residencia'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('municipio_residencia') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-4 col-lg-4">
        <label>{{ trans('labels.direccion_residencia') }}</label>
        <div class="input-group input-group-alternative mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-certificate"></i></span>
            </div>
            <input placeholder="{{ trans('labels.direccion_residencia') }}" id="direccion" type="text" class="form-control{{ $errors->has('direccion') ? ' is-invalid' : '' }}" name="direccion" value="{{ old('direccion') }}" required>

            @if ($errors->has('direccion'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('direccion') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>

<script>

    async function getMunicipios() {

        var id_departamento = document.getElementById('departamento_residencia').value;

        await fetch('{{ url("obtener_municipio") }}/' + id_departamento)
            .then(response => response.json())
            .then(data => {
                document.getElementById('municipio_residencia').innerHTML = "";
                for (const municipio of data) {
                    var opcion = document.createElement('option');
                    opcion.value = municipio.id;
                    opcion.innerHTML = municipio.nombre;
                    document.getElementById('municipio_residencia').appendChild(opcion);
                }
            });
    }

</script>