<?php

/** Traducciones usadas en para titulos. */

return [

    'welcome' => '¡Bienvenido!',
    'start_with_credentials' => 'Inicie con sus credenciales',
    'email' => 'Correo electrónico',
    'confirm_email' => 'Confirmar correo electrónico',
    'password' => 'Contraseña',
    'confirm_password' => 'Confirmar contraseña',
    'remember' => 'Recuérdame',
    'login' => 'Iniciar sesión',
    'forgot_password' => 'Olvidó contraseña',
    'register' => 'Registro',
    'regist' => 'Registrar',
    'user' => 'Usuario',
    'expert' => 'Experto',
    'fill_data_expert' => 'Para trabajar con nosotros debes llenar la siguiente información.',
    'fill_data_user' => 'Para empezar usar nuestros servicios, debes llenar la siguiente información.',
    'user_information' => 'Información de usuario',
    'personal_information' => 'Información personal',
    'knowledge' => 'Conocimientos',
    'type_document' => 'Tipo de identificación',
    'number_document' => 'Número de identificación',
    'genre' => 'Género',
    'language' => 'Idioma',
    'date_born' => 'Fecha de nacimiento',
    'town' => 'Barrio',
    'phone' => 'Teléfono',
    'department' => 'Departamento',
    'municipio' => 'Municipio',
    'direccion_residencia' => 'Dirección residencia',
    'telefono_fijo' => 'Teléfono fijo',
    'telefono_celular' => 'Teléfono celular',
    'grupo_sanguineo' => 'Grupo sanguineo',
    'area_desempenar' => 'Area desempeñar',
    'nivel_academico' => 'Nivel academico',

    'general' => [
        'name' => 'Nombre',
        'names' => 'Nombres',
        'last_name' => 'Apellido',
        'last_names' => 'Apellidos',
        
    ]

];